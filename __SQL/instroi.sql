/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50629
Source Host           : localhost:3306
Source Database       : instroi

Target Server Type    : MYSQL
Target Server Version : 50629
File Encoding         : 65001

Date: 2017-03-09 12:13:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for brickets
-- ----------------------------
DROP TABLE IF EXISTS `brickets`;
CREATE TABLE `brickets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name1` varchar(255) DEFAULT NULL,
  `text1` text,
  `name2` varchar(255) DEFAULT NULL,
  `text2` text,
  `name3` varchar(255) DEFAULT NULL,
  `text3` text,
  `image` varchar(255) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brickets
-- ----------------------------
INSERT INTO `brickets` VALUES ('1', 'Топливные брикеты', 'Древесные топливные брикеты (стандарта Pini-Kay)', '100% натуральный продукт, получаемый путём прессования древесных отходов', 'Топливные пеллеты (диаметр до 25 мм)', 'Обеспечивает сыпучесть и позволяет использовать все известные способы автоматизации в подающих устройствах котельных', 'Дрова сырые колотые (производство Украина)', 'Условиях поставки - FСA-Украина', '076b118a3b2f2c180e29caadcaf8274f.png', null, null);

-- ----------------------------
-- Table structure for callback
-- ----------------------------
DROP TABLE IF EXISTS `callback`;
CREATE TABLE `callback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `sort` int(10) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of callback
-- ----------------------------
INSERT INTO `callback` VALUES ('1', 'Алексей', '+380951417543', '127.0.0.1', '1', '0', '1475587650', '1475587650');
INSERT INTO `callback` VALUES ('2', 'Денис', '+380951417543', '127.0.0.1', '1', '0', '1475645908', '1475648327');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` longtext,
  `updated_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `values` longtext,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `config_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `config_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `config_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `config_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', 'E-mail для отправки писем с сайта', 'koshey08@yandex.ru', '0', '1', '0', 'sender', '1', '', '1');
INSERT INTO `config` VALUES ('2', 'Имя латынницей (отображается в заголовке письма)', 'Bonus-skill system', '0', '1', '1', 'title', '1', '', '1');
INSERT INTO `config` VALUES ('3', 'SMTP сервер', 'smtp.yandex.ru', '0', '1', '2', 'smtp_server', '1', '', '1');
INSERT INTO `config` VALUES ('4', 'Логин', 'koshey08@yandex.ru', '0', '1', '3', 'smtp_login', '1', '', '1');
INSERT INTO `config` VALUES ('5', 'Пароль', 'ko23SH08ey93', '0', '1', '4', 'smtp_password', '4', '', '1');
INSERT INTO `config` VALUES ('6', 'Тип подключения', 'ssl', '0', '1', '5', 'smtp_sequre', '3', '[{\"key\":\"TLS\",\"value\":\"tls\"},{\"key\":\"SSL\",\"value\":\"ssl\"}]', '1');
INSERT INTO `config` VALUES ('7', 'Порт. Например 465 или 587. (587 по умолчанию)', '465', '0', '1', '6', 'smtp_port', '1', '', '1');
INSERT INTO `config` VALUES ('8', 'E-mail администратора', 'osadlhs@mail.ru', '0', '1', '1', 'admin_email', '1', '', '2');
INSERT INTO `config` VALUES ('9', 'Количество строк в админ панеле', '10', '0', '1', '2', 'admin_count', '1', null, '2');
INSERT INTO `config` VALUES ('10', 'Адрес предприятия', 'г.Херсон, ул. Советская, 28', '0', '1', '3', 'address', '1', null, '2');
INSERT INTO `config` VALUES ('11', 'Номер телефона в шапке сайта №1', '+38 (0552) 26-36-18', '0', '1', '4', 'phone1', '1', null, '2');
INSERT INTO `config` VALUES ('12', 'Номер телефона в шапке сайта №2', '+38 (099) 53-75-19', '0', '1', '5', 'phone2', '1', null, '2');
INSERT INTO `config` VALUES ('13', 'Номер телефона в шапке сайта №3', '+38 (099) 53-75-19', '0', '1', '6', 'phone3', '1', null, '2');
INSERT INTO `config` VALUES ('14', 'Доменно имя сайта', 'instroi.loc', null, '1', '6', 'domen', '1', null, '2');

-- ----------------------------
-- Table structure for config_groups
-- ----------------------------
DROP TABLE IF EXISTS `config_groups`;
CREATE TABLE `config_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config_groups
-- ----------------------------
INSERT INTO `config_groups` VALUES ('1', 'Почта', 'email', '1', '1');
INSERT INTO `config_groups` VALUES ('2', 'Базовые', 'basic', '1', '0');

-- ----------------------------
-- Table structure for config_types
-- ----------------------------
DROP TABLE IF EXISTS `config_types`;
CREATE TABLE `config_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config_types
-- ----------------------------
INSERT INTO `config_types` VALUES ('1', 'Однострочное текстовое поле', 'input');
INSERT INTO `config_types` VALUES ('2', 'Текстовое поле', 'textarea');
INSERT INTO `config_types` VALUES ('3', 'Выбор значения из списка', 'select');
INSERT INTO `config_types` VALUES ('4', 'Пароль', 'password');
INSERT INTO `config_types` VALUES ('5', 'Радио кнопка', 'radio');
INSERT INTO `config_types` VALUES ('6', 'Текстовое поле c редактором', 'tiny');

-- ----------------------------
-- Table structure for control
-- ----------------------------
DROP TABLE IF EXISTS `control`;
CREATE TABLE `control` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `alias` varchar(32) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `other` text,
  `image` varchar(255) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control
-- ----------------------------
INSERT INTO `control` VALUES ('1', 'Главная страница', 'Главная страница АШ1', 'Главная страница', 'Главная страница КЕЙВОРДС', 'Главная страница ДЕСКРИПТИОН', '<p>Древесные топливные брикеты (стандарта Pini-Kay)</p>\r\n<p>100% натуральный продукт, получаемый путём прессования древесных отходов.Процесс производства древесного топливного брикета осуществляется на прессах без добавления каких-либо химических закрепителей, клеев: в качестве связующего, клеящего вещества под воздействием высоких температур и давления является натуральный компонент лигнин, который содержится в клетках древесины, а так же и других растений.Главным преимуществом топливных брикетов является их экологичность.</p>\r\n<ul>\r\n<li>Зольность - 0,8%</li>\r\n<li>Общая сера - 0,06%</li>\r\n<li>Выход летучих веществ - 85,7%</li>\r\n<li>Низшая теплота сгорания - 4390ккал</li>\r\n<li>Высшая теплота сгорания - 4956 ккал</li>\r\n</ul>\r\n<p>Топливные пеллеты</p>\r\n<p>представляют собой прессованные цилиндры диаметром до 25 мм, наибольшее распространение получили пеллеты диаметром 6-10 мм. Подобная форма обеспечивает сыпучесть и позволяет использовать все известные способы автоматизации в подающих устройствах котельных. Во многом благодаря этому, пеллеты стали основным видом прессованного топлива в Европе. Позволяет производить эффективную транспортировку на большие расстояния любым видом транспорта: авто, ж/д, водным, как в упаковке, так и насыпью.</p>', 'index', '1', null, null, null, '1489043356');

-- ----------------------------
-- Table structure for cron
-- ----------------------------
DROP TABLE IF EXISTS `cron`;
CREATE TABLE `cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `subject` longtext,
  `text` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cron
-- ----------------------------

-- ----------------------------
-- Table structure for digest
-- ----------------------------
DROP TABLE IF EXISTS `digest`;
CREATE TABLE `digest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `message` longtext,
  `short_text` longtext,
  `status` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `from_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `from_user` (`from_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of digest
-- ----------------------------

-- ----------------------------
-- Table structure for mail_templates
-- ----------------------------
DROP TABLE IF EXISTS `mail_templates`;
CREATE TABLE `mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` longtext,
  `updated_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mail_templates
-- ----------------------------
INSERT INTO `mail_templates` VALUES ('1', 'Заказ обратного звонка', 'Новый заказ звонка с сайта {{site}}', '<p>Здравствуйте! Вы получили новый заказ звонка с сайта {{site}}. Контактные данные. которые указал пользователь:</p>\r\n<p>&nbsp;</p>\r\n<p>Имя: {{name}}.</p>\r\n<p>Телефон: {{phone}}.</p>\r\n<p>&nbsp;</p>\r\n<p>С Ув. Администрация сайта {{site}}.</p>', '1475645666', '1', '0');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `count` varchar(50) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '0', 'Настройки', 'config', '9999', '1', 'fa-cog', null, 'config', null);
INSERT INTO `menu` VALUES ('2', '0', 'Шаблоны писем', 'mail-templates', '9998', '1', ' fa-envelope', null, 'mail-templates', null);
INSERT INTO `menu` VALUES ('3', '0', 'Системные страницы', 'control', '1', '1', 'fa-folder', null, 'control', null);
INSERT INTO `menu` VALUES ('8', '0', 'Заказы звонка', 'callback/index', '6', '1', ' fa-phone', 'callback', 'callback', null);
INSERT INTO `menu` VALUES ('9', '0', 'Меню', '', '2', '1', 'fa-bars', null, 'sitemenu', null);
INSERT INTO `menu` VALUES ('10', '9', 'Список пунктов меню', 'sitemenu/index', '1', '1', null, null, 'sitemenu', 'index');
INSERT INTO `menu` VALUES ('11', '9', 'Добавить пункт меню', 'sitemenu/create', '2', '1', null, null, 'sitemenu', 'create');

-- ----------------------------
-- Table structure for power_station
-- ----------------------------
DROP TABLE IF EXISTS `power_station`;
CREATE TABLE `power_station` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `string1` varchar(255) DEFAULT NULL,
  `string2` varchar(255) DEFAULT NULL,
  `string3` varchar(255) DEFAULT NULL,
  `string4` varchar(255) DEFAULT NULL,
  `string5` varchar(255) DEFAULT NULL,
  `string6` varchar(255) DEFAULT NULL,
  `string7` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `unit1` varchar(255) DEFAULT NULL,
  `unit2` varchar(255) DEFAULT NULL,
  `unit3` varchar(255) DEFAULT NULL,
  `unit4` varchar(255) DEFAULT NULL,
  `unit5` varchar(255) DEFAULT NULL,
  `unit6` varchar(255) DEFAULT NULL,
  `unit7` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of power_station
-- ----------------------------
INSERT INTO `power_station` VALUES ('1', 'Электростанция на биотопливе', '19,5', '8,0', '5,5', '37000', '66400', '45650', '8300', '1', null, null, 'МВт/ч', 'МВт/ч', 'МВт/ч', 'т/г', 'МВт/ч/г', 'МВт/ч/г', 'ч/г', 'ce8f6a685b185c8a99944272005075c5.png');

-- ----------------------------
-- Table structure for sitemenu
-- ----------------------------
DROP TABLE IF EXISTS `sitemenu`;
CREATE TABLE `sitemenu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sitemenu
-- ----------------------------
INSERT INTO `sitemenu` VALUES ('4', 'Главная', '1', '', '1489048025', '1489048025');
INSERT INTO `sitemenu` VALUES ('5', 'Культура', '1', 'articles/kultura', '1489048105', '1489048105');
INSERT INTO `sitemenu` VALUES ('6', ' Политика', '1', 'articles/politika', '1489048146', '1489048146');
INSERT INTO `sitemenu` VALUES ('7', 'Спорт', '1', 'sport', '1489049095', '1489049095');
INSERT INTO `sitemenu` VALUES ('8', 'О проекте', '1', 'about', '1489049142', '1489049142');
INSERT INTO `sitemenu` VALUES ('9', 'Контакты', '1', 'contacts', '1489049157', '1489049157');

-- ----------------------------
-- Table structure for substrats
-- ----------------------------
DROP TABLE IF EXISTS `substrats`;
CREATE TABLE `substrats` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `string` varchar(255) DEFAULT NULL,
  `short_text1` text,
  `short_text2` text,
  `text1` text,
  `text2` text,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of substrats
-- ----------------------------
INSERT INTO `substrats` VALUES ('1', null, 'Универсальная торфосмесь', '<p>Субстраты</p>\r\n<p>Это готовые к использованию грунтовые смеси на основе верхового и низинного торфа, с добавлением комплекса минеральных микроудобрений и микроэлементов, а также различных дополнительных компонентов. В составе наших грунтов используются <strong>удобрения PG-MIX.</strong></p>', '<p>В соответствии с необходимой рецептурой, вышеуказанные элементы и добавки используются в различных количествах , с целью получения максимально благоприятного состава для произрастания того или иного растения. Основной особенностью субстрата является то, что он представляет собой готовый грунт, а не добавку, которую необходимо смешивать и добавлять в местные грунты, рискуя не достичь необходимого состава и нанести ущерб культивируемому растению.</p>', '<p>Древесные топливные брикеты (стандарта Pini-Kay)</p>\r\n<p>100% натуральный продукт, получаемый путём прессования древесных отходов.Процесс производства древесного топливного брикета осуществляется на прессах без добавления каких-либо химических закрепителей, клеев: в качестве связующего, клеящего вещества под воздействием высоких температур и давления является натуральный компонент лигнин, который содержится в клетках древесины, а так же и других растений.Главным преимуществом топливных брикетов является их экологичность.</p>\r\n<ul>\r\n<li>Зольность - 0,8%</li>\r\n<li>Общая сера - 0,06%</li>\r\n<li>Выход летучих веществ - 85,7%</li>\r\n<li>Низшая теплота сгорания - 4390ккал</li>\r\n<li>Высшая теплота сгорания - 4956 ккал</li>\r\n</ul>\r\n<p>Топливные пеллеты</p>\r\n<p>представляют собой прессованные цилиндры диаметром до 25 мм, наибольшее распространение получили пеллеты диаметром 6-10 мм. Подобная форма обеспечивает сыпучесть и позволяет использовать все известные способы автоматизации в подающих устройствах котельных. Во многом благодаря этому, пеллеты стали основным видом прессованного топлива в Европе. Позволяет производить эффективную транспортировку на большие расстояния любым видом транспорта: авто, ж/д, водным, как в упаковке, так и насыпью.</p>', '<p>Объём, занимаемый пеллетами при сопоставимой теплотворности, в 5 раз ниже, чем у щепы, и равен объему угля.Благодаря сушке в процессе производства, пеллеты биологически неактивны, т.е. не гниют в течение длительного времени и не теряют своих свойств.</p>\r\n<p>Дрова производство Украина</p>\r\n<p>Дрова колотые твердолиственных пород: граб (дуб, ясень - под заказ) <br /> Дрова сырые колотые, упакованные в ящики 2RM (18-19 шт. в машине)</p>\r\n<ul>\r\n<li>Влажность - 25%</li>\r\n<li>Длина полена - 30-33 см</li>\r\n<li>Ширина раскола - 6-18 см</li>\r\n</ul>\r\n<p>Упаковка:</p>\r\n<ol>\r\n<li>Ящики 2RM (22 шт. в машине)<br /> Ящики 1,8RM (24 шт. в машине)</li>\r\n<li>Сетка в ящиках 2RM (в одном ящике - 96 сеток, в машине - 2112 сеток)</li>\r\n</ol>\r\n<p>Дрова сырые колотые, упакованные в ящики 2RM (18-19 шт. в машине), Условиях поставки - FСA-Украина.</p>', 'bbb50517fa3d3a62573233d03ca1ccdb.png', '1', null, null);

-- ----------------------------
-- Table structure for transport
-- ----------------------------
DROP TABLE IF EXISTS `transport`;
CREATE TABLE `transport` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `string` varchar(255) DEFAULT NULL,
  `text1` text,
  `text2` text,
  `status` tinyint(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transport
-- ----------------------------
INSERT INTO `transport` VALUES ('1', 'Транспортные перевозки по Европе', 'Объемная масса торфа колеблется в широких пределах от 0,2 т/м3 до 0,65 т/м3', '<p>Его перевозят в специализированных цельнометаллических полувагонах-хопперах с объемом кузова 120 м3 и грузоподъемностью 58 т или в универсальных полувагонах с наращенными бортами. Масса торфа определяется взвешиванием на вагонных весах или обмером! Его перевозят в специализированных цельнометаллических полувагонах-хопперах с объемом кузова 120 м3 и грузоподъемностью 58 т или в универсальных полувагонах с наращенными бортами. Масса торфа определяется взвешиванием на вагонных весах или обмером!</p>', '<p>Масса торфа определяется взвешиванием на вагонных весах или обмером! Его перевозят в специализированных цельнометаллических полувагонах-хопперах с объемом кузова 120 м3 и грузоподъемностью 58 т или в универсальных полувагонах с наращенными бортами. Масса торфа определяется взвешиванием на вагонных весах или обмером!</p>', '1', null, null, 'ddff70f4ee98721c21f9c5cd1cf1d895.png');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `education` longtext,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', 'Lfx372rV3Bm4wSNelUiCTR7tINFOvrvD', '$2y$13$P3k5TqT3FV794.XjVnC/.OZcJzFqDSvIyGn1/KcIzuhT9M3LClJQS', null, 'admin@gmail.com', '1', '1471584794', '1475563636', '1', '8971bce0e1a03626710e4d71ac4c797d.jpg', 'Смоляк Алексей', null, null);
SET FOREIGN_KEY_CHECKS=1;
