$(document).ready(function($) {

            
        
// READY
// ==========

	$(window).load(function () {
		// ========== SLIDER ==========//
		$('.single-item').slick({
			prevArrow: '<img class="slider__arrowLeft" src="images/arrow-left.png">',
			nextArrow: '<img class="slider__arrowRight" src="images/arrow-right.png">',
		}); 
	})
	
	// ========== MENU ==========//
    $('.menu-media__lang').on('click', function(e) {
        $('.menu-media__lang-div').toggleClass('open');
        var firstClick = true;
        $(document).on('click.myEvent', function(e) {
            if (!firstClick && $(e.target).closest('.open').length == 0) {
                $('.open').removeClass('open');
                $(document).off('click.myEvent');
            }
            firstClick = false;
        });
        e.preventDefault();
    });

    $('.menu-media__call').on('click', function(e) {
        api.openPanel($('#numbers'));
        api.open();
    });

	// ========== VIDEO ==========//
	$('.popup-youtube').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade video-fullscreen',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false,
		iframe: {
			patterns: {
				youtube: {
					index: 'youtube.com/',
					id: 'v=',
					src: '//www.youtube.com/embed/%id%?autoplay=1&modestbranding=1&autohide=1&showinfo=0&iv_load_policy=3'
				}
			}
		} 
	});

	


	// ========== POPUP'S ==========//
	$('.popup-with-form').magnificPopup({
		type: 'inline',
        fixedContentPos: false,
        closeBtnInside: true,
        removalDelay: 300,
        mainClass: 'zoom-in'
	});	

	// ========== GALLERY ========== //

	$('.portfolio__tile').each(function(){
		$(this).magnificPopup({
			delegate: 'a',
			type: 'image',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1]
			}
		})
	});

	// ========== ACCORDION ==========//
	var accordionsMenu = $('.faqs-list');

	if( accordionsMenu.length) {
		
		accordionsMenu.each(function(i,el){
			var accordion = $(this);
			accordion.on('click', '.faqs-list__title', function(){
				var element = $(this).parent()
				var content = element.children('p.faqs-list__description');
				element.toggleClass("active");
				content.slideToggle();
			})
		});
	};

	// ========== MAP ========== //
	$('#map').each(function(index, el) {
        var lat = $(el).data('lat');
        var lng = $(el).data('lng');
        var zoom = $(el).data('zoom') || 16;
        initMap(el, lat, lng, zoom);
    }); 


 	var checkMap = $('#map');
 	if( checkMap.length ) {
	ymaps.ready(function (lat, lng, zoom) {

	    var myMap = new ymaps.Map('map', {
            center: [50.352709, 30.293960],
            zoom: 14,
            controls: ['geolocationControl', 'zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'INSTROI',
            balloonContent: 'INSTROI'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'images/marker.png',
            iconImageSize: [60, 75],
            iconImageOffset: [-30, -75]
        });

	    myMap.geoObjects.add(myPlacemark);
	});
	}




	// ========== TABS ========== //
	// $('ul.portfolio__menu').on('click', 'li:not(.active)', function() {console.log('qqq');
	// $(this)
	// 	.addClass('active').siblings().removeClass('active')
	// 	.closest('.portfolio').find('div.portfolio__block').removeClass('active').eq($(this).index()).addClass('active');
	// });




	// ========== FIXED MENU ========== //
	function getScrollTop() {
               var scrOfY = 0;
               if( typeof( window.pageYOffset ) == "number" ) {
                       
                       scrOfY = window.pageYOffset;
               } else if( document.body
               && ( document.body.scrollLeft
               || document.body.scrollTop ) ) {
                       
                       scrOfY = document.body.scrollTop;
               } else if( document.documentElement
               && ( document.documentElement.scrollLeft
                || document.documentElement.scrollTop ) ) {
                       
                       scrOfY = document.documentElement.scrollTop;
               }
               return scrOfY;
    }
    $(window).scroll(function() {
        fixPaneRefresh();
    });
     
    function fixPaneRefresh(){
        if ($(".head-menu").length) {
            var top  = getScrollTop();

            if (top < 136) $(".head-menu").removeClass("fixedMenu"),
            			   $(".head-menu li").removeClass("scroll"),
            			   $("#menu-button-scroll").css("display","none");
            else $(".head-menu").addClass("fixedMenu"),
            	 $(".head-menu li").addClass("scroll"),
                 $("#menu-button-scroll").css("display","block");
        }
    }




    // ========== MMENU ========== //
   
    $("#menu").mmenu({  
        navbar: {
	        title: "Меню"
	    },          
        "extensions": [
            "pagedim-black"
        ],
        "counters": true,
        onClick: {
            close: true,
        },
        clone: false,
    });

    var api = $("#menu").data( "mmenu" );








	// подержка css анимаций
	var CssTransitionSupport = Modernizr.cssanimations;



		wHTML.validation();
// onLOAD
// ==========

	$(window).load(function() {
		/* Act on the event */
	});




// onRESIZE
// ==========

	$(window).resize(function(event) {
		/* Act on the event */
	});




// onSCROLL
// ==========

	$(window).scroll(function(event) {
		/* Act on the event */
	});


});