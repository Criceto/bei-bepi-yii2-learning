<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $js = [
        'plugins/jQuery/jquery-2.2.3.min.js',
        'media/js/bootstrap.js',
        'media/js/noty/jquery.noty.packaged.min.js',
        'media/js/programmer/my.js',
        'media/js/programmer/Preloader.js',
        'plugins/iCheck/icheck.min.js',
        'plugins/morris/morris.js',
        'plugins/sparkline/jquery.sparkline.min.js',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'plugins/knob/jquery.knob.js',
        'plugins/daterangepicker/moment.js',
        'plugins/daterangepicker/daterangepicker.js',
        'plugins/datepicker/bootstrap-datepicker.js',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
        'plugins/fastclick/fastclick.js',
        'media/js/select2/select2.js',
        'media/js/select2/ru.js',
        'media/js/dashboard.js',
        'media/js/demo.js',
        'media/js/app.js',
        'media/js/programmer/wAjax.js',
        'media/js/tinymce/tinymce.min.js'
    ];

    public $css = [
        'media/css/bootstrap.css',
        'media/css/animate.css',
        'plugins/iCheck/square/blue.css',
        'media/css/programmer/my.css',
        'media/css/programmer/Preloader.css',
        'media/css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        'plugins/iCheck/all.css',
        'plugins/morris/morris.css',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'plugins/datepicker/datepicker3.css',
        'plugins/daterangepicker/daterangepicker.css',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'media/css/select2.css',
        'media/css/AdminLTE.css',
    ];

    public $depends = [
        'yii\web\YiiAsset', // yii.js, jquery.js
//        'yii\bootstrap\BootstrapAsset', // bootstrap.css
//        'yii\bootstrap\BootstrapPluginAsset' // bootstrap.js
    ];

    public $jsOptions = [
        'position' =>  View::POS_HEAD,
    ];
}
