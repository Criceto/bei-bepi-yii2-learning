<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $updated_at
 * @property integer $status
 * @property integer $sort
 * @property string $key
 * @property integer $type_id
 * @property string $values
 * @property integer $group_id
 *
 * @property ConfigGroups $group
 * @property ConfigTypes $type
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'values'], 'string'],
            [['updated_at', 'status', 'sort', 'type_id', 'group_id'], 'integer'],
            [['name', 'key'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConfigGroups::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConfigTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'sort' => 'Sort',
            'key' => 'Key',
            'type_id' => 'Type ID',
            'values' => 'Values',
            'group_id' => 'Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(ConfigGroups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ConfigTypes::className(), ['id' => 'type_id']);
    }

    public static function getRows(){
        $result =  self::find()->where(['status' => 1])->orderBy(['sort' => SORT_ASC])->all();

        $rows = array();
        foreach ($result as $obj){
            $rows[$obj->group_id][] = $obj;
        }
        
        return $rows;
    }
}
