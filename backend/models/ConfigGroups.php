<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "config_groups".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $status
 * @property integer $sort
 *
 * @property Config[] $configs
 */
class ConfigGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'sort'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'status' => 'Status',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigs()
    {
        return $this->hasMany(Config::className(), ['group_id' => 'id']);
    }
    
    public static function getRows(){
        return self::find()->join('JOIN','config',self::tableName().'.id =config.group_id')->orderBy(['sort' => SORT_ASC])->groupBy(self::tableName().'.id')->all();
    }
}
