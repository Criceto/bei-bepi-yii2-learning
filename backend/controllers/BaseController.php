<?php
namespace  backend\controllers;

use frontend\components\Email;
use yii\helpers\Url;
use yii\web\Controller;

class BaseController extends Controller{

    public function beforeAction($action)
    {
        define('HOST',$_SERVER['DOCUMENT_ROOT']);
        //Если пользователь авторизован, то перенаправляем на главную без формы входа
        if(!\Yii::$app->user->id) {
            if (\Yii::$app->controller->action->id != 'login' && \Yii::$app->controller->action->id != 'login-post') {
                if (!substr_count($_SERVER['REQUEST_URI'], 'post')) {
                    \Yii::$app->session->set('redirect', $_SERVER['REQUEST_URI']);
                }
                $this->redirect(Url::toRoute('/site/login'), 301);
                return false;
            }
        }

        return parent::beforeAction($action);
    }

    public function success($params = array()){
        if(is_array($params)){
            $params += ['success' => true];
            echo json_encode($params);
            die;
        } else{
            echo json_encode([
                'jquery' => [
                    [
                        'action' => 'message',
                        'type' => 'success',
                        'text' => $params
                    ]
                ]
            ]);
        }
        die;
    }

    public function error($params=array()){
        if(is_array($params)){
            $params += ['success' => false];
            echo json_encode($params);
            die;
        } else{
            echo json_encode([
                'jquery' => [
                    [
                        'action' => 'message',
                        'type' => 'error',
                        'text' => $params
                    ]
                ]
            ]);
        }
        die;
    }

}