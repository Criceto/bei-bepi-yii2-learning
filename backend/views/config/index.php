<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo Yii::$app->view->title;?>
        </h1>
        <?php echo \backend\components\Breadcrumbs::generateBreadcrumbs();?>
    </section>
    <section class="content">
        <div class="row">
        <div class="col-md-12">
            <?php
            $form = \yii\bootstrap\ActiveForm::begin([
                'action' => '/config/config-post',
                'options' => [
                    'class' => 'wForm',
                    'enctype' => 'multipart/form-data'
                ]
            ]);?>
            <?php if(count($config_groups)){?>
                    <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <?php foreach ($config_groups as $key=>$obj){?>
                            <li class="<?php echo $key == 0 ? 'active' : '';?>">
                                <a href="#<?php echo $obj->alias;?>" data-toggle="tab" aria-expanded="true"><?php echo $obj->name;?></a></li>
                        <?php }?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach ($config_groups as $key=>$obj){?>
                            <div class="tab-pane <?php echo $key == 0 ? 'active' : '';?>" id="<?php echo $obj->alias;?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php foreach ($rows[$obj->id] as $j){?>
                                        <div class="form-group">
                                            <label for="<?php echo $j->key;?>" class="control-label"><?php echo $j->name;?></label>
                                            <?php echo \backend\components\Widgets::get('Admin_Config_Field', [
                                                'obj' => $j,
                                                'group' => $obj
                                            ]);?>
                                        </div>                                            
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <?php echo \backend\components\Widgets::get('Admin_Buttons_Submit');?>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                    </div>
            <?php }?>
            <?php \yii\bootstrap\ActiveForm::end(); ?>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
    </section>
</div>