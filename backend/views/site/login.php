<div class="login_page">
    <div class="login-box">
        <div class="login-logo">
            <a href="/"><b>Bei-bepi</b> system</a>
        </div>

        <div class="login-box-body login-form">
            <p class="login-box-msg">Войдите для использования системы</p>
            <?
            $form = \yii\bootstrap\ActiveForm::begin([
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => '/site/login-post',
                'options' => [
                    'class' => 'wForm'
                ]
            ]);?>
            <div class="form-group has-feedback">
                <?php echo $form->field($model, 'email')->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'E-mail',
                    'type' => 'email'
                ])->label(false);?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->field($model, 'password')->passwordInput([
                    'class' => 'form-control',
                    'placeholder' => 'Пароль',
                ])->label(false);?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <?php echo $form->field($model,'rememberMe')->checkbox()->label('Запомнить меня');?>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <?php echo \yii\bootstrap\Html::submitButton('Войти',[
                        'class' => 'btn btn-primary btn-block btn-flat'
                    ]);?>
                </div>
                <!-- /.col -->
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
</div>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>