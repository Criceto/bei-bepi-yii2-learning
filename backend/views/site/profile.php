<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo Yii::$app->view->title;?>
        </h1>
        <?php echo \backend\components\Breadcrumbs::generateBreadcrumbs();?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- /.col -->
                <?php echo \backend\components\Widgets::get('User_Profile_Form', [
                    'profile_model' => $profile_model,
                    'user' => $user
                ]);?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>