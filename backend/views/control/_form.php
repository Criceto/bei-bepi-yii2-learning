<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Control */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'action' => '/control/update-post?id='.$model->id,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'options' => [
            'class' => 'wForm',
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(
        [
            'maxlength' => true,
            'disabled' => 'disabled'
        ]
    ) ?>

    <?= $form->field($model, 'h1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea([
        'rows' => 12,
        'class' => 'tinymceEditor'
    ])?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
