<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo Yii::$app->view->title; ?>
        </h1>
        <?php echo \backend\components\Breadcrumbs::generateBreadcrumbs(); ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="box box-primary">
                            <div class="box-header">
                                <i class="ion ion-clipboard"></i>

                                <h3 class="box-title">Список шаблонов</h3>

                                <!--<div class="box-tools pull-right">
                                    <ul class="pagination pagination-sm inline">
                                        <li><a href="#">&laquo;</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">&raquo;</a></li>
                                    </ul>
                                </div>-->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul class="todo-list">
                                    <?php foreach ($dataProvider->getModels() as $obj){?>
                                        <li data-id="<?php echo $obj->id;?>">
                                            <!-- drag handle -->
                                              <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                              </span>
                                            <span class="text">
                                                <a href="<?php echo \yii\helpers\Url::to('/mail-templates/update?id='.$obj->id);?>">
                                                    <?php echo $obj->name;?>
                                                </a>
                                            </span>
                                            <!-- General tools such as edit or delete-->
                                            <div class="tools" style="font-size: 19px">
                                                <?php echo \backend\components\Widgets::get('Status', [
                                                    'status' => $obj->status,
                                                    'id' => $obj->id
                                                ]);?>
                                                <a title="Редактировать" href="<?php echo \yii\helpers\Url::to('/mail-templates/update?id='.$obj->id);?>" class="label label-info"><i class="fa fa-edit"></i> Редактировать</a>
                                            </div>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix no-border">
<!--                                <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<input type="hidden" id="table-name" value="<?php echo $tableName;?>">

<script>
    $(function () {

        "use strict";

        $(".todo-list").sortable({
            placeholder: "sort-highlight",
            handle: ".handle",
            forcePlaceholderSize: true,
            zIndex: 999999,
            stop: function(event, ui){
                var ids = [];
                var i = 0;
                ui.item.closest('.todo-list').find('li').each(function () {
                    ids[i] = $(this).attr('data-id');
                    i++;
                });

                $.ajax({
                    url: '/ajax/change-sort',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        ids : ids,
                        tableName: $('#table-name').val()
                    },
                    success: function(data){
                        if(!data.success){
                            generate('Что-то пошло не так, перезагрузите страницу!', 'error');
                        }
                    }
                });
            }
        });
    });
</script>