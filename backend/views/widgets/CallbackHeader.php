<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-envelope-o"></i>
        <span class="label label-success header-count-digest-count"><?php echo count($messages);?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header header-count-digest-text">Новых уведомлений: <?php echo count($messages);?></li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php if(count($messages)){?>
                    <?php foreach ($messages as $obj){?>
                        <li><!-- start message -->
                    <a class="js-popupItem" href="<?php echo \yii\helpers\Url::to('/callback/update?id='.$obj->id);?>">
                        <h4>
                            <?php echo $obj->name;?>
                            <small><i class="fa fa-clock-o"></i> <?php echo \common\components\Dates::timeAgo($obj->created_at);?></small>
                        </h4>
                        <p><?php echo $obj->phone;?></p>
                    </a>
                </li>
                    <?php }?>
                <?php }?>
            </ul>
        </li>
        <li class="footer"><a href="#" class="see-all-callback">Отметить все как прочитанные</a></li>
    </ul>
</li>