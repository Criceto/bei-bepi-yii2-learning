<?php echo \kato\DropZone::widget([
    'options' => [
        'paramName' => $name,
        'url' => $uploadUrl,
        'maxFilesize' => '2',
        'maxFiles' => $maxFiles,
        'addRemoveLinks' => true,
        'dictDefaultMessage' => 'Выберите файл для загрузки',
        'dictRemoveFile' => 'Удалить файл',
        'dictMaxFilesExceeded' => 'Для профиля можно загрузить только одно фото',
        'dictCancelUpload' => 'Отменить загрузку',
        'dictCancelUploadConfirmation' => 'Вы уверены что хотите отменить загрузку?',
        'dictFileTooBig' => 'Вы загрузили слишком большой файл'
    ],
    'clientEvents' => [
        'complete' => "function(file){
                            $('#myDropzone').get(0).dropzone.options.maxFiles = $('#myDropzone').get(0).dropzone.options.maxFiles - 1;
                                if($('#myDropzone').get(0).dropzone.options.maxFiles == 0){
                                    $('#myDropzone').get(0).dropzone.disable();
                                    $('.dz-message').hide();
                                }
            }",
        'success' => "function(file, response){
                            var resp = JSON.parse(response);
                            file.newName= resp.name;                                                     
                            wAjax.start(resp.jquery, $('body'));
                        }",
        'removedfile' => "function(file){
                            $.ajax({
                            url: '".$removeUrl."',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                name: file.newName || file.name,
                               '_csrf-backend': '".Yii::$app->request->csrfToken."'
                            },
                            success: function (data) {
                                wAjax.start(data.jquery, $('body'));
                                
                                $('#myDropzone').get(0).dropzone.enable();
                                $('.dz-message').show();
                                $('#myDropzone').get(0).dropzone.options.maxFiles = $('#myDropzone').get(0).dropzone.options.maxFiles + 1;
                            }
                        });
                    }",
        'sending' => $sending,
    ],
]);?>