<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo \yii\helpers\Url::toRoute('/admin/index/index');?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>BSS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Bonus-skill system</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Скрыть навигацию</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php echo \frontend\components\widgets\Widgets::get('Digest');?>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if(is_file(HOST.Yii::getAlias('@web/media/images/user/small/'.Yii::$app->user->identity->image))){?>
                            <img src="<?php echo \yii\helpers\Url::to('@web/media/images/user/small/'.Yii::$app->user->identity->image,true);?>" class="user-image user-avatar" alt="User Image">
                        <?php } else {?>
                            <img src="<?php echo \yii\helpers\Url::to('@web/media/pic/avatar.png');?>" class="user-image user-avatar" alt="User Image">
                        <?php }?>
                        <span class="hidden-xs user-name"><?php echo Yii::$app->user->identity->name ? Yii::$app->user->identity->name : Yii::$app->user->identity->email;?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php if(is_file(HOST.Yii::getAlias('@web/media/images/user/small/'.Yii::$app->user->identity->image))){?>
                                <img src="<?php echo \yii\helpers\Url::to('@web/media/images/user/small/'.Yii::$app->user->identity->image,true);?>" class="img-circle user-avatar" alt="User Image">
                            <?php } else {?>
                                <img src="<?php echo \yii\helpers\Url::to('@web/media/pic/avatar.png');?>" class="img-circle user-avatar" alt="User Image">
                            <?php }?>

                            <p>
                                <span class="user-name"><?php echo Yii::$app->user->identity->name ? Yii::$app->user->identity->name : Yii::$app->user->identity->email;?></span>
                                <small class="position-text"><?php echo Yii::$app->user->identity->position ? Yii::$app->user->identity->position : 'Программист'?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--<li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                        </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo \yii\helpers\Url::toRoute('/admin/admin/profile');?>" class="btn btn-default btn-flat">Профиль</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo \yii\helpers\Url::toRoute('/user/user/logout');?>" class="btn btn-default btn-flat">Выйти</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!--<li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>