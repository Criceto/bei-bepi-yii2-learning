<?php
    switch ($obj->type_id) {
        case 1:
            echo \yii\helpers\Html::input('text', $group->alias.'['.$obj->key.']', $obj->value, [
                    'class' => 'form-control',
                    'placeholder' => $obj->name,
                    'id' => $obj->key
                ]);
             break;
        case 2:
            echo \yii\helpers\Html::textarea($group->alias.'['.$obj->key.']', $obj->value,[
                'class' => 'form-control',
                'placeholder' => $obj->name,
                'id' => $obj->key
            ]);
            break;
        case 3:
            $values = json_decode($obj->values);
            $options = array();
            foreach ($values as $j){
                $options[$j->value] = $j->key;
            }
            echo \yii\helpers\Html::dropDownList($group->alias.'['.$obj->key.']',$obj->value, $options, [
                'class' => 'form-control',
                'id' => $obj->key
            ]);
            break;
        case 4:?>
            <div class="input-group input-group-sm">
                <?php echo \yii\helpers\Html::input('password', $group->alias.'['.$obj->key.']', $obj->value, [
                    'class' => 'form-control password-field',
                    'placeholder' => $obj->name,
                    'id' => $obj->key
                ]);?>
                <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat show-password"><i class="fa fa-eye"></i></button>
                </span>
            </div>
            <?php break;
        case 5:
            $values = json_decode($obj->values);
            $options = array();
            foreach ($values as $j){
                $options[$j->value] = $j->key;
            }
            echo \yii\helpers\Html::radioList($group->alias.'['.$obj->key.']',$obj->value,$options,[
                'class' => 'form-control',
                'id' => $obj->key
            ]);
            break;
        case 6:
            echo \yii\helpers\Html::textarea($group->alias.'['.$obj->key.']',$obj->value,[
                'class' => 'form-control tinymceEditor',
                'id' => $obj->key
            ]);
            break;
    }
?>
