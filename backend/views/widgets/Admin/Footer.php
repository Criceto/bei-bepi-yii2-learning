<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.5
    </div>
    <strong>Copyright &copy; 2016 <a target="_blank" href="http://wezom.com.ua">Wezom</a></strong> агенство системных интернет решений
</footer>