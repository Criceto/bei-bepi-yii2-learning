<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#settings" data-toggle="tab">Настройки</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="settings">
                <?
                $form = \yii\bootstrap\ActiveForm::begin([
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'action' => '/site/profile-post',
                    'options' => [
                        'class' => 'form-horizontal wForm',
                        'enctype' => 'multipart/form-data'
                    ]
                ]);?>

                <div class="">
                    <label for="inputName" class="col-sm-2 control-label">Имя</label>
                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'name')->textInput([
                            'class' => 'form-control',
                            'value' => $user->name,
                            'placeholder' => 'Имя',
                            'type' => 'text'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'email')->textInput([
                            'class' => 'form-control',
                            'value' => $user->email,
                            'placeholder' => 'E-mail',
                            'type' => 'email'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputName" class="col-sm-2 control-label">Пароль</label>

                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'password')->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => 'Пароль',
                            'data-toggle' => 'tooltip',
                            'title' => 'Оставьте поле пустым, если не хотите изменять пароль'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputSkills" class="col-sm-2 control-label">Изображение</label>

                    <div class="col-sm-10">
                        <div class="form-group">
                            <?php echo \backend\components\Widgets::get('Dropzone', [
                                'name' => 'image',
                                'uploadUrl' => \yii\helpers\Url::to('/site/upload-photo'),
                                'maxFiles' => 1,
                                'removeUrl' => \yii\helpers\Url::to('/site/remove-image'),
                                'sending' => 'function(file, xhr_o, data){
                                                   data.append("user_id", '.$user->id.')
                                                }',
                            ]);?>
                        </div>
                        <?php if(is_file(Yii::getAlias('@frontend/web/media/images/user/small/'.$user->image))){?>
                            <div class="dropzone-preview"><?php echo json_encode([
                                    [
                                        'name' => $user->image,
                                        'size' => filesize(Yii::getAlias('@frontend/web/media/images/user/small/'.$user->image)),
                                        'path' => 'http://'.str_replace('admin.','',$_SERVER['HTTP_HOST']).Yii::getAlias('@web/media/images/user/small/'.$user->image)
                                    ]
                                ]);?>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <?php echo $form->field($profile_model, 'id')->hiddenInput([
                    'value' => $user->id
                ])->label(false);?>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <?php echo \yii\helpers\Html::submitButton('Подтвердить',['class' => 'btn btn-danger']);?>
                    </div>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
</div>