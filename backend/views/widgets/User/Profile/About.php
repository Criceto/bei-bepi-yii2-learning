<!-- About Me Box -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Про меня</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i> Образование</strong>

        <p class="text-muted education-text">
            <?php echo $user->education ? $user->education : 'Не указано';?>
        </p>

        <hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Должность</strong>

        <p class="text-muted position-text"><?php echo $user->position ? $user->position : 'Программист';?></p>

        <hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Изученные навыки</strong>

        <p>
            <span class="label label-danger">UI Design</span>
            <span class="label label-success">Coding</span>
            <span class="label label-info">Javascript</span>
            <span class="label label-warning">PHP</span>
            <span class="label label-primary">Node.js</span>
        </p>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->