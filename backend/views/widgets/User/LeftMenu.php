<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php if(is_file(Yii::getAlias('@frontend/web/media/images/user/small/'.Yii::$app->user->identity->image))){?>
                    <img src="<?php echo 'http://'.str_replace('admin.','',$_SERVER['HTTP_HOST']).\yii\helpers\Url::to('@web/media/images/user/small/'.Yii::$app->user->identity->image);?>" class="user-avatar img-circle" alt="User Image">
                <?php } else {?>
                    <img src="<?php echo \yii\helpers\Url::to('@web/media/pic/avatar.png');?>" class="user-avatar img-circle" alt="User Image">
                <?php }?>
            </div>
            <div class="pull-left info">
                <p class="user-name"><?php echo Yii::$app->user->identity->name ? Yii::$app->user->identity->name : Yii::$app->user->identity->email;?></p>
                <a href="<?php echo \yii\helpers\Url::toRoute('/site/profile');?>"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Меню</li>
            <?php if(count($menu)){?>
                <?php echo \backend\components\Widgets::get('RecursiveMenu',
                    [
                        'menu' => $menu,
                        'count' => $count,
                        'module' => 'user',
                        'active_element' => 0
                    ]
                );?>
            <?php }?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>