<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sitemenu */
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo Yii::$app->view->title;?>
        </h1>
        <?php echo \backend\components\Breadcrumbs::generateBreadcrumbs(); ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Основные данные</h3>
                    </div>

                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>

                </div>
            </div>
        </div>
</div>
</section>
</div>