<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo Yii::$app->view->title; ?>
        </h1>
        <?php echo \backend\components\Breadcrumbs::generateBreadcrumbs(); ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="box box-primary">
                            <div class="box-header">
                                <i class="ion ion-clipboard"></i>
                                <h3 class="box-title">Список слайдов</h3>

                                <a href="<?php echo \yii\helpers\Url::toRoute('/slider/create');?>" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Создать</a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul class="todo-list">
                                    <?php foreach ($dataProvider->getModels() as $obj){?>
                                        <li data-id="<?php echo $obj->id;?>">
                                            <!-- drag handle -->
                                              <span class="handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                              </span>
                                            <span class="text">
                                                <?php if(is_file(Yii::getAlias('@frontend/web/media/images/slider/big/'.$obj->image))){?>
                                                    <img style="width: 25%" src="<?php echo 'http://'.str_replace('admin.','',$_SERVER['HTTP_HOST']).Yii::getAlias('@web/media/images/slider/small/'.$obj->image);?>" alt="">
                                                <?php }?>
                                                <a href="<?php echo \yii\helpers\Url::to('/slider/update?id='.$obj->id);?>">
                                                    <?php echo $obj->name. ' ' . $obj->name2;?>
                                                </a>
                                            </span>
                                            <!-- General tools such as edit or delete-->
                                            <div class="tools" style="font-size: 19px">
                                                <?php echo \backend\components\Widgets::get('Status', [
                                                    'status' => $obj->status,
                                                    'id' => $obj->id
                                                ]);?>
                                                <div class="btn-group">
                                                    <a href="<?php echo \yii\helpers\Url::to('/slider/update?id='.$obj->id);?>" class="btn btn-info btn-xs">Редактировать</a>
                                                    <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu text-12px" role="menu">
                                                        <li><a data-pjax="0" data-method="post" data-confirm="Вы уверены?" href="<?php echo \yii\helpers\Url::to('/slider/delete?id='.$obj->id);?>">Удалить</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix no-border">
                                <a href="<?php echo \yii\helpers\Url::toRoute('/slider/create');?>" class="btn btn-default pull-left"><i class="fa fa-plus"></i> Создать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<input type="hidden" id="table-name" value="<?php echo $tableName;?>">

<script>
    $(function () {

        "use strict";

        $(".todo-list").sortable({
            placeholder: "sort-highlight",
            handle: ".handle",
            forcePlaceholderSize: true,
            zIndex: 999999,
            stop: function(event, ui){
                var ids = [];
                var i = 0;
                ui.item.closest('.todo-list').find('li').each(function () {
                    if($(this).attr('data-id')) {
                        ids[i] = $(this).attr('data-id');
                        i++;
                    }
                });

                $.ajax({
                    url: '/ajax/change-sort',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        ids : ids,
                        tableName: $('#table-name').val()
                    },
                    success: function(data){
                        if(!data.success){
                            generate('Что-то пошло не так, перезагрузите страницу!', 'error');
                        }
                    }
                });
            }
        });
    });
</script>