<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'action' => '/slider/'.($model->isNewRecord ? 'create' : 'update').'-post?id='.$model->id,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'options' => [
            'class' => 'wForm',
        ]
    ]); ?>

    <?= $form->field($model, 'status')->radioList([
        0 => 'Нет',
        1 => 'Да'
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php if(!$model->isNewRecord){?>
        <div class="form-group">
            <label for="inputSkills" class="control-label">Изображение</label>
    
    
            <?php echo \backend\components\Widgets::get('Dropzone', [
                'name' => 'image',
                'uploadUrl' => \yii\helpers\Url::to('/slider/upload-photo'),
                'maxFiles' => 1,
                'removeUrl' => \yii\helpers\Url::to('/slider/remove-image'),
                'sending' => 'function(file, xhr_o, data){
                                                       data.append("slider_id", '.$model->id.')
                                                    }',
            ]);?>
    
            <?php if(is_file(Yii::getAlias('@frontend/web/media/images/slider/big/'.$model->image))){?>
                <div class="dropzone-preview"><?php echo json_encode([
                        [
                            'name' => $model->image,
                            'size' => filesize(Yii::getAlias('@frontend/web/media/images/slider/big/'.$model->image)),
                            'path' => 'http://'.str_replace('admin.','',$_SERVER['HTTP_HOST']).Yii::getAlias('@web/media/images/slider/small/'.$model->image)
                        ]
                    ]);?>
                </div>
            <?php }?>
    
        </div>
    <?php }?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Закрыть', \yii\helpers\Url::toRoute('/slider/index'),['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
