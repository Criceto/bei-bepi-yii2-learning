<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Sitemenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'action' => '/sitemenu/'.($model->isNewRecord ? 'create' : 'update').'-post?id='.$model->id,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'options' => [
            'class' => 'wForm',
        ]
    ]); ?>

    <?= $form->field($model, 'status')->radioList([
        0 => 'Нет',
        1 => 'Да'
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput([
        'maxlength' => true,
        'class' => 'form-control url'
    ]) ?>
    <div style="margin-top: -13px; margin-bottom: 15px"><?php echo \common\components\Config::get('basic.domen');?>/<span class="url-destination"><?php echo $model->url;?></span></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Закрыть', \yii\helpers\Url::toRoute('/sitemenu/index'),['class' => 'btn btn-danger']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>