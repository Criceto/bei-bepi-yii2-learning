<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'action' => '/services/update-post?id='.$model->id,
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'class' => 'wForm',
    ]
]); ?>
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Основные данные</h3>
            </div>

            <div class="box-body">

                <?= $form->field($model, 'status')->radioList([
                    0 => 'Нет',
                    1 => 'Да'
                ]); ?>

                <?= $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'class' => 'translitSrc form-control'
                ]) ?>
                
                <?= $form->field($model, 'alias', ['template' => '
                <div class="form-group field-services-alias">
                    {label}
                    <div class="input-group">
                        {input} 
                        <div class="input-group-btn">
                          <button type="button" class="btn btn-danger translit">Заполнить автоматически</button>
                        </div>
                        <!-- /btn-group -->
                    
                    </div>                                 
                    <div class="help-block"></div>
                </div>
                '])->textInput([
                    'maxlength' => true,
                    'class' => 'translitDst form-control'
                ])?>

                <?= $form->field($model, 'text')->textarea([
                    'rows' => 12,
                    'class' => 'tinymceEditor'
                ]) ?>

                <?= $form->field($model, 'text_short')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'text_short2')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>


            </div>

        </div>
    </div>
    <div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Мета-данные</h3>
        </div>

        <div class="box-body">

            <?= $form->field($model, 'h1')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'keywords')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>
