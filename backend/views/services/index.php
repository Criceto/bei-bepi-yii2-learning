<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo Yii::$app->view->title; ?>
        </h1>
        <?php echo \backend\components\Breadcrumbs::generateBreadcrumbs(); ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="box box-primary">
                            <div class="box-header">
                                <i class="ion ion-clipboard"></i>

                                <h3 class="box-title">Список услуг</h3>
                                <a href="<?php echo \yii\helpers\Url::toRoute(['/admin/tickets/create']);?>" class="btn btn-default pull-right ml-7px"><i class="fa fa-plus"></i> Добавить</a>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="width: 10px">№</th>
                                        <th style="width: 30px">Изображение</th>
                                        <th>Название</th>
                                        <th>Краткое описание</th>
                                        <th style="width: 14%">Действия</th>
                                    </tr>
                                    <?php foreach ($dataProvider->getModels() as $key=>$obj){?>
                                        <tr>
                                            <td><?php echo ($key+1);?>.</td>
                                            <td>
                                                <a href="<?php echo \yii\helpers\Url::to('/callback/update?id='.$obj->id);?>"><?php echo $obj->name;?></a>
                                            </td>
                                            <td>
                                                <?php echo $obj->text_short;?>
                                            </td>
                                            <td>
                                                <div class="tools" style="font-size: 19px">
                                                    <?php echo \backend\components\Widgets::get('Status', [
                                                        'status' => $obj->status,
                                                        'id' => $obj->id
                                                    ]);?>
                                                    <a title="Редактировать" href="<?php echo \yii\helpers\Url::to('/services/update?id='.$obj->id);?>" class="label label-info"><i class="fa fa-edit"></i> Редактировать</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody></table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix no-border">
                                <a href="<?php echo \yii\helpers\Url::toRoute(['/services/create']);?>" class="btn btn-default pull-left ml-7px"><i class="fa fa-plus"></i> Добавить</a>
                                <?php echo $pager;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<input type="hidden" id="table-name" value="<?php echo $tableName;?>">
