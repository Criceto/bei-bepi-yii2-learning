window.wAjax = (function (window, document, undefined) {
    var wa = Object.create(null),
        b = $('body');

    /**
     * Method that try to do anything
     * @param options
     * @param form
     */
    wa.start = function (options, form) {
        if (!options) {
            return false;
        }
        $.each(options, function (key, value) {
            if (value.action && wa[value.action] != undefined) {
                wa[value.action](value, form);
            }
        });
    };

    /**
     * PopUp message success|warning
     * @param options [text, type, time]
     * @returns {boolean}
     */
    wa.message = function (options) {
        console.log(options);
        if (!options.text) {
            return false;
        }
        var time = options.time || 3000;
        var type = options.type || 'default';
        generate(options.text, type, time);
    };

    /**
     * Reset form to start
     * @param options
     * @param form
     */
    wa.reset_form = function (options, form) {
        if(form == undefined){
            form = $(options.selector).length ? $(options.selector): undefined;
        }
        if (options.reset && form != undefined) {
            form.validReset();
        }
    };

    /**
     * Redirect user to somewhere or reload page
     * @param options
     */
    wa.redirect = function (options) {
        if (window.location.href == options.link) {
            window.location.reload();
        } else {
            window.location.href = options.link;
        }
    };

    /**
     * Insert part of HTML in DOM
     * @param options
     */
    wa.insertion = function (options) {
        if (!options.selector || options.content == undefined) {
            return false;
        }
        var method = options.method || 'html';
        switch (method) {
            case 'prependTo':
            case 'insertAfter':
            case 'insertBefore':
                $(options.content)[method](options.selector);
                break;
            case 'append':
            case 'prepend':
            case 'after':
            case 'before':
            case 'appendTo':
            case 'text':
            case 'html':
            case 'replaceWith':
                $(options.selector)[method](options.content);
                break;
        }
    };

    /**
     * Remove, empty, detach, unwrap element
     * @param options
     * @returns {boolean}
     */
    wa.remove = function (options) {
        if (!options.selector) {
            return false;
        }
        var method = options.method || 'remove';
        switch (method) {
            case 'empty':
            case 'detach':
            case 'unwrap':
            case 'remove':
                b.find(options.selector)[method]();
                break;
        }
    };

    /**
     * Manipulations with attributes
     * @param options
     * @returns {boolean}
     */
    wa.attributes = function (options) {
        if (!options.selector || options.value === undefined) {
            return false;
        }
        var method = options.method || 'toggleClass';
        switch (method) {
            case 'addClass':
            case 'removeAttr':
            case 'removeClass':
            case 'toggleClass':
            case 'height':
            case 'width':
                $(options.selector)[method](options.value);
                break;
            case 'attr':
            case 'prop':
            case 'data':
                console.log(options.selector);
                console.log(options.attribute);
                console.log( options.value);  

                $(options.selector)[method](options.attribute, options.value);
                break;
        }
    };

    /**
     * Styles to element
     * @param options
     * @returns {boolean}
     */
    wa.css = function (options) {
        if (!options.selector || !options.styles) {
            return false;
        }
        $(options.selector).css(options.styles);
    };

    /**
     * Hide, show, toggle
     * @param options
     * @returns {boolean}
     */
    wa.visibility = function (options) {
        if (!options.selector) {
            return false;
        }
        var method = options.method || 'toggle';
        var duration = options.time || 'slow';
        switch (method) {
            case 'hide':
            case 'show':
            case 'toggle':
                b.find(options.selector)[method]();
                break;
            case 'fadeIn':
            case 'fadeOut':
            case 'fadeToggle':
            case 'slideUp':
            case 'slideDown':
            case 'slideToggle':
                b.find(options.selector)[method](duration);
                break;
        }
    };

    /**
     * Animation
     * @param options
     * @returns {boolean}
     */
    wa.animate = function (options) {
        if (!options.selector) {
            return false;
        }
        var config = options.config || {};
        var time = options.time || 500;
        b.find(options.selector).animate(config, time, function () {});
    };

    /**
     * Magnific popup
     * @param options
     */
    wa.magnific = function (options) {
        var method = options.method || 'close';
        switch (method) {
            case 'close':
                $.magnificPopup.close();
                break;

            case 'open':
                $.magnificPopup.open({
                    items: [{
                        src: options.content, // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    }],
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-slide-bottom',
                    callbacks: {
                        open: function () {
                            wHTML.validation();
                            datepickInit('.js-datetimepicker-popup');
                        }
                    }
                });
                break;
        }
    };

    /**
     * Initialize validation again
     */
    wa.validation = function () {
        wHTML.validation();
    };

    return wa;
})(this, this.document);
