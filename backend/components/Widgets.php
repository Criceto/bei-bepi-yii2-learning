<?php
namespace backend\components;

use app\models\Digest;
use common\components\Notify;
use common\models\Brickets;
use common\models\Callback;
use common\models\Control;
use common\models\PowerStation;
use common\models\Substrats;
use common\models\Transport;
use yii\base\Component;
use yii\db\Query;

class Widgets extends Component{

    static $_instance; // Constant that consists self class

    public $_data = array(); // Array of called widgets
    public $_substrats;

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public static function get($name, $params=array()){
        $w = Widgets::factory();
        $path = str_replace('_','/',$name);
        if(method_exists($w, $name)){
            $result = $w->{$name}($params);
            return \Yii::$app->view->render('@app/views/widgets/'.$path, $result ? $result : array());
        } else {
            return \Yii::$app->view->render('@app/views/widgets/'.$path, $params ? $params : array());
        }
    }

    public function Noty(){
        return Notify::getMessage();
    }

    public function User_LeftMenu(){
        $punkts = (new Query())
            ->select([
                'menu.*'
            ])
            ->from('menu')
            ->where(['status' => 1])
            ->orderBy([
                'sort' => SORT_ASC
            ])
            ->all();
        $menu = array();
        foreach ($punkts as $obj){
            $menu[$obj['id_parent']][] = $obj;
        }

        $count_callback = Callback::find()->where(['status' => 0])->count();

        $count = [
            'callback' => $count_callback
        ];

        return [
            'menu' => $menu,
            'count' => $count
        ];
    }

    public function CallbackHeader(){

        $callback = Callback::find()->where(['status' => 0])->orderBy(['created_at' => SORT_ASC])->all();

        return [
            'messages' => $callback
        ];
    }

    public function HiddenData(){

        $model = Control::findOne(['id' => 1]);

        return [
          'model' => $model
        ];
    }
    
}