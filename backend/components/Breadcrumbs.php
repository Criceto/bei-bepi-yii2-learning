<?php
namespace backend\components;

use yii\base\Component;

class Breadcrumbs extends Component{
    static $_instance; // Constant that consists self class
    
    public $_breadcrumbs = array();
    
    public $item_template = '<li><a href="{{link}}">{{name}}</a></li>';
    
    public $last_item_template = '<li>{{name}}</li>';

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }
    
    public static function setBread($name, $link='', $template = ''){
        self::factory()->_breadcrumbs[] = [
            'itemTemplate' => $template ? $template : self::factory()->item_template,
            'name' => $name,
            'link' => $link
        ];
        
        return true;
    }

    public static function generateBreadcrumbs(){
        $breads = self::factory()->_breadcrumbs;

        if(count($breads)){
        $tpl = '<ol class="breadcrumb">';
            foreach ($breads as $key=>$obj){
                $template = $obj['itemTemplate'];

                //послений элемент хлебных крошек
                if($key == (count($breads)-1)){
                    $template = ($obj['itemTemplate'] == self::factory()->item_template) ? self::factory()->last_item_template : $obj['item_template'];
                }

                $template = str_replace(array(
                    '{{link}}',
                    '{{name}}'
                ),array($obj['link'],$obj['name']), $template);
                $tpl .= $template;
            }
            $tpl .= '</ol>';
            return $tpl;
        }

        return '';
    }
}