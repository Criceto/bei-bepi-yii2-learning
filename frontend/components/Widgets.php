<?php
namespace frontend\components;

use app\models\Digest;
use common\components\Notify;
use common\models\Brickets;
use common\models\Callback;
use common\models\Control;
use common\models\PowerStation;
use common\models\Substrats;
use common\models\Transport;
use yii\base\Component;
use yii\db\Query;

class Widgets extends Component{

    static $_instance; // Constant that consists self class

    public $_data = array(); // Array of called widgets
    public $_substrats;

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public static function get($name, $params=array()){
        $w = Widgets::factory();
        $path = str_replace('_','/',$name);
        if(method_exists($w, $name)){
            $result = $w->{$name}($params);
            return \Yii::$app->view->render('@app/views/widgets/'.$path, $result ? $result : array());
        } else {
            return \Yii::$app->view->render('@app/views/widgets/'.$path, $params ? $params : array());
        }
    }

    public function Noty(){
        return Notify::getMessage();
    }
    
}