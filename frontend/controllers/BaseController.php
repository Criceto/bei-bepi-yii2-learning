<?php
namespace  frontend\controllers;

use common\components\Config;
use common\components\Email;
use yii\helpers\Url;
use yii\web\Controller;

class BaseController extends Controller{

    public function beforeAction($action)
    {
        define('HOST',$_SERVER['DOCUMENT_ROOT']);

        return parent::beforeAction($action);
    }

    public function success($params = array()){
        if(is_array($params)){
            $params += ['success' => true];
            echo json_encode($params);
            die;
        } else{
            echo json_encode([
                'jquery' => [
                    [
                        'action' => 'message',
                        'type' => 'success',
                        'text' => $params
                    ]
                ]
            ]);
        }
        die;
    }

    public function error($params=array()){
        if(is_array($params)){
            $params += ['success' => false];
            echo json_encode($params);
            die;
        } else{
            echo json_encode([
                'jquery' => [
                    [
                        'action' => 'message',
                        'type' => 'error',
                        'text' => $params
                    ]
                ]
            ]);
        }
        die;
    }

}