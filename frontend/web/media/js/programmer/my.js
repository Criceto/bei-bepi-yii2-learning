var generate = function( message, type, time ) {
    var timeout = time;
    if(!timeout){
        timeout = 3000;
    }
    var n = noty({
        text: message,
        type: type,
        animation: {
            open: 'animated flipInY', // Animate.css class names
            close: 'animated flipOutY' // Animate.css class names
        },
        speed: 500,
        timeout: timeout
    });
};


$(function () {
    
    $('.reg-but').on('click',function (e) {
        e.preventDefault();
        $(this).closest('.login-form').slideUp(500, function () {
            $('.registration-form').slideDown(500);
        });
    });

    $('.login-page-back').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.login-box-body').slideUp(500, function () {
            $('.login-form').slideDown(500);
        });
    });

    $('.forgot-password-but').on('click',function (e) {
        e.preventDefault();
        $(this).closest('.login-form').slideUp(500, function () {
            $('.forgot-form').slideDown(500);
        });
    });

    $(document).ready(function () {

        if($('.dropzone-preview').length) {

            var previews = JSON.parse($('.dropzone-preview').text());

            $.each(previews, function (key, obj) {
                // console.log(obj);
                // Create the mock file:
                var mockFile = {name: obj.name, size: obj.size};

                // Call the default addedfile event handler
                $('#myDropzone').get(0).dropzone.emit("addedfile", mockFile);

                // And optionally show the thumbnail of the file:
                $('#myDropzone').get(0).dropzone.emit("thumbnail", mockFile, obj.path);
                // Or if the file on your server is not yet in the right
                // size, you can let Dropzone download and resize it
                // callback and crossOrigin are optional.
                $('#myDropzone').get(0).dropzone.createThumbnailFromUrl(mockFile, obj.path);

                // Make sure that there is no progress bar, etc...
                $('#myDropzone').get(0).dropzone.emit("complete", mockFile);
            });
        }
    });

    //#костыль по авторизации через соц сети
    $('.btn-facebook').on('click', function (event) {
        event.preventDefault();
        $('.ulogin-button-vkontakte').trigger('click')
    });

    $('.btn-google').on('click', function (event) {
        event.preventDefault();
        $('.ulogin-button-google').trigger('click')
    });
    //---------------------------------------------------------------------------------

    //рекурсивное добавление актиных класов для левоо меню
    var activep = $('body').find('.sidebar-menu').find('.active');
    function recursiveActive(activePunkt) {
        if(activePunkt.closest('.sidebar-menu').length){
            activePunkt.addClass('active');
            if(activePunkt.parent().closest('.treeview').length){
                return recursiveActive(activePunkt.parent().closest('.treeview'));
            }
            return true;
        }

        return true;
    }
    recursiveActive(activep);
    //---------------------------------------------------------------------------------------

    //кнопка просмотра пароля на странице настроек сайта
    $('.show-password').mousedown(function(){
        $(this).closest('.input-group').find('.password-field').attr('type','text');
    });
    $('.show-password').mouseup(function(){
        $(this).closest('.input-group').find('.password-field').attr('type','password');
    });
    //--------------------------------------------------------------------------------------------
    
    
    //инициализация тиньки
    if($('.tinymceEditor').length){
        tinymce.init({
            selector: "textarea.tinymceEditor",
            skin : "wezom",
            language : 'ru',
            plugins: [
                "advlist autolink lists link image charmap print preview hr",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern responsivefilemanager"
            ],
            table_class_list: [
                {title: 'Поумолчанию', value: ''},
                {title: 'Без границ', value: 'table-null'},
                {title: 'Зебра', value: 'table-zebra'}
            ],
            image_class_list: [
                {title: 'Поумолчанию', value: ''}
            ],
            toolbar1: "undo redo pastetext | bold italic forecolor backcolor fontselect fontsizeselect styleselect | alignleft aligncenter alignright alignjustify",
            toolbar2: 'bullist numlist outdent indent | link unlink image responsivefilemanager fullscreen',
            image_advtab: true,
            external_filemanager_path:"/media/js/tinymce/filemanager/",
            filemanager_title:"Менеджер файлов" ,
            external_plugins: { "filemanager" : "filemanager/plugin.min.js"},
            document_base_url: "http://"+window.location.hostname+"/",
            convert_urls: false,
            plugin_preview_width: "1000",
            relative_urls: false,
            default_language:'ru',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
        });

        var wLastSmall, wLastBig;
        if($(window).width() > 700){
            wLastSmall = false;
            wLastBig = true;
        }else{
            wLastSmall = true;
            wLastBig = false;
        }

        $(window).on('resize',function(){

            if($(window).width() > 700 && wLastSmall){
                wLastSmall = false;
                wLastBig = true;
                parent.tinyMCE.activeEditor.windowManager.close(window);
            }
            if($(window).width() < 700 && wLastBig){
                wLastSmall = true;
                wLastBig = false;
                parent.tinyMCE.activeEditor.windowManager.close(window);
            }
        })
    }
    //---------------------------------------------------------------------------------------------


    //измение статуса публикации
    $('body').on('click', '.status-change', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var tableName = $('#table-name').val();
        var status;
        if($(this).hasClass('label-success')){
            status = 0;
        } else {
            status = 1;
        }
        var t = $(this);

        $.ajax({
            url: '/ajax/change-status',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id : id,
                tableName: tableName,
                status: status
            },
            success: function(data){
                if(data.success){
                    if(status == 1){
                        t.removeClass('label-danger');
                        t.addClass('label-success');
                        t.attr('title', 'Опубликовано');
                        t.find('.fa').removeClass('fa-thumbs-o-down');
                        t.find('.fa').addClass('fa-thumbs-o-up');
                    } else {
                        t.removeClass('label-success');
                        t.addClass('label-danger');
                        t.attr('title', 'Не опубликовано');
                        t.find('.fa').removeClass('fa-thumbs-o-up');
                        t.find('.fa').addClass('fa-thumbs-o-down');
                    }
                } else {
                    generate('Что-то пошло не так, перезагрузите страницу!', 'error');
                }
            }
        });
    });
    //-----------------------------------------------------------------------------------

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"], input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    
    //-------------------------------------------------------------------------------------
    
    //Инициализация select2
    $('.sl2').select2();
    //--------------------------------------------------------------------------------------
    
    
    //изменение обязательности уровня
    $('.levels_select').on('change', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var status = $(this).val();

        $(this).removeClass('select_green');
        $(this).removeClass('select_red');
        $(this).removeClass('select_yellow');

        if(status == 0){
            $(this).addClass('select_red');
        }
        if(status == 1){
            $(this).addClass('select_green');
        }
        if(status == 2){
            $(this).addClass('select_yellow');
        }

        $.ajax({
            url: '/ajax/change-skill-level-status',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id : id,
                status: status
            },
            success: function(data){
              if(data.error){
                  generate('Что-то пошло не так, перезагрузите страницу!','error');
              }
            }
        });  
    });
    //---------------------------------------------------------------------------------------

    
    $('#skill_group').on('change', function () {
        var id = $(this).val();
        $.ajax({
            url: '/ajax/get-skills-groups',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id : id
            },
            success: function(data){
                if (data.tpl) {
                    $('#skill').closest('.field-tickets-skill_id').show();
                    data.tpl = '<option value="">Выберите навык</option>' + data.tpl;
                    $('#skill').html(data.tpl);
                    $('#skill').select2();
                } else {
                    $('#skill').closest('.field-tickets-skill_id').hide();
                    $('#skill').html('');
                }

            }
        });
    });
    
    //-----------------------------------------------------------------------------------------------
    
    //клик по актиному сообщению в хедере
    $('.digest_item').on('click',function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var href = $(this).attr('href');
        var t = $(this);
        $.ajax({
            url: '/ajax/read-digest-message',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id : id
            },
            success: function(data){
                if(href == '#'){
                    $('.header-count-digest-count').text(data.count);
                    $('.header-count-digest-text').text('Новых уведомлений: ' + data.count);
                    if(data.count == '0'){
                        $('.see-all-digest').slideUp(500, function () {
                            $('.see-all-digest').remove();
                        });
                    }
                    t.closest('li').slideUp(500, function () {
                        t.closest('li').remove();
                    });
                } else {
                    window.location.href = href;
                }
            }
        });
        return false;
    })
});