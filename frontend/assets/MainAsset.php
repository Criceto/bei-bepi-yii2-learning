<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $js = [
        'media/js/vendor/modernizr.js',
        'media/js/vendor/jquery.magnific-popup.js',
        'media/js/vendor/jquery.mmenu.all.min.js',
        'media/js/vendor/slick.js',
        'media/js/vendor/jquery-validation.js',
        'media/js/vendor/validation.js',
        'media/js/vendor/wold.js',
        'media/js/init.js',
    ];

    public $css = [
        'media/css/animate.css',
        'media/css/programmer/Preloader.css',
        'media/css/vendor/normalize.css',
        'media/css/vendor/slick.css',
        'media/css/vendor/slick-theme.css',
        'media/css/fonts/fonts.css',
        'media/css/style.css',
        'media/css/media.css',
        'media/css/vendor/magnific-popup.css',
        'media/css/vendor/jquery.mmenu.all.css',
    ];

    public $depends = [
        'yii\web\YiiAsset', // yii.js, jquery.js
        'yii\bootstrap\BootstrapAsset', // bootstrap.css
        'yii\bootstrap\BootstrapPluginAsset' // bootstrap.js
    ];

    public $jsOptions = [
        'position' =>  View::POS_END,
    ];
}
