<div style="display: none;">
    <div id="orderCall" class="orderCall zoomAnim">
        <div class="enterReg_top">
            <div class="popupBlock enterBlock wCur">
                <div class="erTitle">Чтобы мы связались с вами, <span>заполните форму</span>
                </div>
                <div class="popupContent">
                    <div id="entrForm2" data-ajax="<?php echo \yii\helpers\Url::to('/site/callback-post');?>" data-form="true" class="wForm enterBlock_form visForm">
                        <div class="wFormRow">
                            <input type="text" name="name" id="name" placeholder="Введите свое имя" data-rule-multiword="true" data-msg-multiword="Введите корректное имя" data-msg-required="Это поле необходимо заполнить" required="">
                            <div class="inpInfo">Имя</div>
                        </div>
                        <!-- .wFormRow -->
                        <div class="wFormRow">
                            <input type="tel" class="tel" name="phone" id="enter_tel" placeholder="Введите свой телефон" data-msg-required="Это поле необходимо заполнить" data-rule-phone="true" data-msg-phone="Пожалуйста, введите верный телефон" required="">
                            <div class="inpInfo">Телефон</div>
                        </div>

                        <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->csrfToken;?>">

                        <!-- .wFormRow -->
                        <div class="tal">
                            <button class="wSubmit btn btn_green"><span>Отправить заявку</span>
                            </button>
                        </div>
                    </div>
                    <span class="order_call_info">Наши менеджеры свяжутся с вами в ближайшее время</span>

                </div>
            </div>
            <!-- .enterBlock -->
        </div>
        <!-- .enterReq_top -->
    </div>
    <!-- #enterReq -->

    <?php if($model){?>
        <div id="enterTxt" class="enterRegPopup pop_up_text zoomAnim">
        <div class="enterReg_top">
            <div class="popupBlock enterBlock wCur">
                <div class="popupContent wTxt">
                    <div class="pop_up_text_l">
                        <?php echo $model->text1;?>
                    </div>
                    <div class="pop_up_text_r">
                        <?php echo $model->text2;?>
                    </div>

                </div>
            </div>
            <!-- .enterBlock -->
        </div>
        <!-- .enterReg_top -->
    </div>
    <?php }?>
    <!-- #enterReg -->
</div>