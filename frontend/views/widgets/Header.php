<header class="page-header">

    <div class="head-upper">
        <div class="page-size">
            <div class="head-upper__inner">

                <div class="head-upper__location">
                    Мы работаем по всей Украине
                </div>


                <div class="head-upper__rightSide">

                    <div class="wrapper-lang">
                        <div class="wrapper-lang__active">
                            <img class="lang-flag" src="/media/pic/lang-ukr.png"><span>Укр<span> <img src="/media/pic/lang-select.png">
                        </div>

                        <div class="wrapper-lang__choose">
                            <ul>
                                <li><a href="#"><img class="lang-flag" src="/media/pic/lang-ukr.png"><span>Укр</span></a></li>
                                <li><a href="#"><img class="lang-flag" src="/media/pic/lang-rus.png"><span>Рус</span></a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="social">

                        <a class="social__link--vk" href="#">
                            <svg class="social__link" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26" height="26" viewBox="0 0 26 26">
                                <circle cx="13" cy="13" r="12.5"/>
                                <image id="Shape_25" data-name="Shape 25" x="4.5" y="9.5" width="16" height="9" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAMAAAAM9FwAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAnFBMVEWAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5UAAAALzilsAAAAMnRSTlMAgpmFIVjw/OU9ZZNyw6UDz20i/r1C/Sitqfkyu1BcMMb1igEGjxHHhjdsCHvZTD/nyXS33xcAAAABYktHRDM31XxeAAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH4AkQCik45kombwAAAHJJREFUCNc9jFkOgkAAQ8vI4i4qoLggCu7i8u5/OGcwsR9NX9NUnun4CsKo25P6ZjDUCMaaxDDVbA6JUsgWWkKgHFZrKYGNW2wLoJC0i6C0Od5DLqcDf5nKFXX5I3vJsZ2czo4v19D6rW3ujybjqdcbPl9DzBBhL/acUAAAAABJRU5ErkJggg=="/>
                            </svg>
                        </a>

                        <a class="social__link--fb" href="#">
                            <svg class="social__link" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26" height="26" viewBox="0 0 26 26">
                                <image id="Shape_26" data-name="Shape 26" x="10.5" y="7.5" width="6" height="12" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAMBAMAAACzedEdAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAKlBMVEWAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5UAAABeyqD6AAAADHRSTlMAAYnmO2EqEXPu0WkVSVKuAAAAAWJLR0QN9rRh9QAAAAlwSFlzAAALEgAACxIB0t1+/AAAAAd0SU1FB+AJEAoqGYEOZfIAAAAlSURBVAjXY2BUPsPAcuYMA+uZdIaKM+UMZ86cBOJTDNxnGLBhAJg6D02EGZBWAAAAAElFTkSuQmCC"/>
                                <circle cx="13" cy="13" r="12.5"/>
                            </svg>
                        </a>

                        <a class="social__link--google" href="#">
                            <svg class="social__link" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26" height="26" viewBox="0 0 26 26">
                                <circle cx="13" cy="13" r="12.5"/>
                                <image id="Shape_27" data-name="Shape 27" x="8.5" y="7.5" width="12" height="12" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAMAAABhq6zVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAxlBMVEWAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5WAi5UAAABVbbQhAAAAQHRSTlMADHpz2feLJHBUsksX8ogIRJuGIC6izxDAG/54hVqT6y8UsxgSPYRO+bpIbY7IBnS3BDj48GSsSsQFGaTbsZKCQO9orgAAAAFiS0dEQYnebE4AAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgCRAKKjTE0TmHAAAAe0lEQVQI1z2K5xLBQAAGN4hwONESEcTpLdF7y/s/lTjG9+Ob3ZkFI5XOmFmLXB4QBYolWbYrVZtaXIeG48qmkHgtH9oC/E6SdYOe6lswGCbCaBxP+M+bzowfzuViaYaR5tV6A9vdN9wfPn88aTmHCpS4aLne7o/ny9X8BoGeCafw0MLRAAAAAElFTkSuQmCC"/>
                            </svg>
                        </a>

                    </div>
                </div>

            </div>

        </div>
    </div>


    <div class="head-lower">
        <div class="page-size">
            <div class="head-lower__inner">

                <div class="head-lower__logo">
                    <a href="index.html"><img src="/media/pic/logo.png"></a>
                </div>

                <div class="head-lower__description">
                    Утепление пеноизолом <br>
                    заливочным пенопластом
                </div>

                <div class="head-lower__order-but">
                    <a href="#popup-count" class="popup-with-form"><span>Заказать расчёт</span></a>
                </div>

                <div class="head-lower__call">
                    <div class="head-lower__call--popup">
                        <a href="#popup-callback" class="popup-with-form">Перезвоните мне</a>
                    </div>
                    <div class="head-lower__call-numbers">
                        <div class="menu-number">
                            <img src="/media/pic/lang-select.png">

                            <a href="tel:+380960000157">
                                <div class="menu-number__icon">
                                    <img src="/media/pic/number.svg">
                                </div>
                                <div class="menu-number__number">+38(096) <span>0000 157</span></div>
                            </a>
                        </div>

                        <div class="menu-number__hidden">
                            <div class="menu-number menu-number--hidden">
                                <a href="tel:+380950000157">
                                    <div class="menu-number__icon">
                                        <img src="/media/pic/mts.jpg">
                                    </div>
                                    <div class="menu-number__number">+38(095) <span>0000 157</span></div>
                                </a>
                            </div>

                            <div class="menu-number menu-number--hidden">
                                <a href="tel:+380676254579">
                                    <div class="menu-number__icon">
                                        <img src="/media/pic/kiev.jpg">
                                    </div>
                                    <div class="menu-number__number">+38(067) <span>6254 579</span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="header-callback">
                    <a href="#popup-callback" class="header-callback popup-with-form">
                        <svg  xmlns="http://www.w3.org/2000/svg" width="50.34" height="50.657" viewBox="0 0 50.34 50.657">
                            <path d="M1544.5,59.4a24.881,24.881,0,0,1,24.74.362,25.527,25.527,0,0,1,12.48,20.777,25.935,25.935,0,0,1-7.4,18.985,25.3,25.3,0,0,1-21.88,7.04c-2.81-.362-5.32-1.7-7.97-2.568-3.57-.393-7.15.423-10.71,0.645,0.17-3.384.85-6.738,0.84-10.132-1.42-3.857-3.21-7.684-3.23-11.894A25.572,25.572,0,0,1,1544.5,59.4m1.8,3.908c-9.53,5.187-13.67,18.35-8.26,27.928,1.7,2.558.64,5.65,0.47,8.47,2.81-.03,5.9-1.017,8.49.5a20.181,20.181,0,0,0,17.05.967A21.259,21.259,0,0,0,1577.34,83.7a20.993,20.993,0,0,0-7.06-17.9,20.648,20.648,0,0,0-23.98-2.5m-1.7,14.231c-1.11-3.4,1.07-7.715,4.85-7.9,1.94,1.471,2.68,4.069,4.09,6.013-1.03,1.41-3.44,2.407-2.84,4.492,1.68,3.485,4.79,5.932,8,7.916,1.31-1.078,2.53-2.266,3.79-3.4,1.97,1.239,3.97,2.447,5.99,3.606a5.88,5.88,0,0,1-7.12,5.489,23.109,23.109,0,0,1-16.76-16.215h0Z" transform="translate(-1531.38 -56.281)"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>



    <div class="menu-media">

        <div class="menu-media__menu">
            <div class="menu-media__box">
                <a class="call-menu" href="#menu">
                    <img src="/media/pic/menu.svg">
                </a>
                <span class="menu-media__title">меню</span>
            </div>
            <div class="menu-media__menu-div">
                <div id="menu">
                    <ul>
                        <li><a href="index.html">Главная</a></li>
                        <li><a href="02_uslugi.html">Услуги</a></li>
                        <li><a href="04_penoizol.html">О Пеноизоле</a></li>
                        <li><a href="06_news.html">Статьи</a></li>
                        <li><a href="07_faqs.html">Вопрос/Ответ</a></li>
                        <li><a href="08_portfolio.html">Портфолио</a></li>
                        <li><a href="09_price.html">Прайс-лист</a></li>
                        <li><a href="#">О компании</a></li>
                        <li><a href="10_contacts.html">Контакты</a></li>
                        <li><span>Телефоны</span></a>
                            <ul id="numbers">
                                <li><a href="#">+38(096) 0000 157</a></li>
                                <li><a href="#">+38(095) 0000 157</a></li>
                                <li><a href="#">+38(067) 6254 579</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="menu-media__call">
            <div class="menu-media__box">
                <img src="/media/pic/phone-call.svg">
                <span class="menu-media__title">звонок</span>
            </div>
            <div class="menu-media__call-div">
                <div class="menu-media__number">
                    <a href="tel:+380676254579">+38(067) <span>6254 579</span></a>
                </div>

                <div class="menu-media__number">
                    <a href="tel:+380950000157">+38(095) <span>0000 157</span></a>
                </div>

                <div class="menu-media__button">
                    <a href="#popup-callback" class="popup-with-form">Перезвоните мне</a>
                </div>
            </div>
        </div>

        <div class="menu-media__lang">
            <div class="menu-media__box">
                <img src="/media/pic/earth.svg">
                <span class="menu-media__title">язык</span>
            </div>
            <div class="menu-media__lang-div">
                <ul>
                    <li><a href="#"><img class="lang-flag" src="/media/pic/lang-ukr.png"><span>Укр</span></a></li>
                    <li><a href="#"><img class="lang-flag" src="/media/pic/lang-rus.png"><span>Рус</span></a></li>
                </ul>
            </div>
        </div>

        <div class="menu-media__calc">
            <div class="menu-media__box">
                <a href="#popup-count" class="popup-with-form"><img src="/media/pic/calculator.svg"></a>
                <span class="menu-media__title">расчёт</span>
            </div>
        </div>

    </div>

    <div class="head-menu">
        <div class="page-size">
            <div class="head-menuInner">
                <ul>
                    <li><a href="index.html">Главная</a></li>
                    <li><a href="02_uslugi.html">Услуги</a></li>
                    <li><a href="04_penoizol.html">О Пеноизоле</a></li>
                    <li><a href="06_news.html">Статьи</a></li>
                    <li><a href="07_faqs.html">Вопрос/Ответ</a></li>
                    <li><a href="08_portfolio.html">Портфолио</a></li>
                    <li><a href="09_price.html">Прайс-лист</a></li>
                    <li><a href="#">О компании</a></li>
                    <li><a href="10_contacts.html">Контакты</a></li>
                </ul>

                <div class="menu-button-scroll" id="menu-button-scroll">
                    <a href="#popup-count" class="popup-with-form"><span>Заказать расчёт</span></a>
                </div>
            </div>
        </div>
    </div>

</header>