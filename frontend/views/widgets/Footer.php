<footer class="wFooter">
    <div class="wSize">
        <div class="logo fll">
            <img src="<?php echo \yii\helpers\Url::to('@web/media/pic/hd_logo.png',true);?>" height="57" width="60" alt="">
        </div>
        <ul>
            <li class="address">
                <span><?php echo \common\components\Config::get('basic.address');?></span>
            </li>
            <li class="mail">
                            <span>
                                <a href="mailto:<?php echo \common\components\Config::get('basic.admin_email');?>"><?php echo \common\components\Config::get('basic.admin_email');?></a>
                            </span>
            </li>
            <li class="phones">
                            <span>
                                <a href="tel:<?php echo preg_replace('/[^0-9]/','',\common\components\Config::get('basic.phone1'));?>"><?php echo \common\components\Config::get('basic.phone1');?></a>
                                <span class="hide_phones">
                                    <a href="tel:<?php echo preg_replace('/[^0-9]/','',\common\components\Config::get('basic.phone2'));?>"><?php echo \common\components\Config::get('basic.phone2');?></a>
                                    <a href="tel:<?php echo preg_replace('/[^0-9]/','',\common\components\Config::get('basic.phone3'));?>"><?php echo \common\components\Config::get('basic.phone3');?></a>
                                </span>
                                <b></b>
                            </span>
                <a href="#orderCall" class="pop_up">Заказать звонок</a>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
</footer>