<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\MainAsset;
use common\widgets\Alert;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!--[if IE]><meta http-equiv="imagetoolbar" content="no"><![endif]-->
    <!-- saved from url=(0014)about:internet -->
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">

    <!-- Touch -->
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">

    <!-- Responsive -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="target-densitydpi=device-dpi">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo Yii::getAlias('@web/media/favicons/apple-touch-icon.png');?>">
    <link rel="icon" type="image/png" href="<?php echo Yii::getAlias('@web/media/favicons/favicon-32x32.png');?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo Yii::getAlias('@web/media/favicons/favicon-16x16.png');?>" sizes="16x16">
    <link rel="manifest" href="<?php echo Yii::getAlias('@web/media/favicons/manifest.json');?>">
    <link rel="mask-icon" href="<?php echo Yii::getAlias('@web/media/favicons/safari-pinned-tab.svg');?>" color="#5bbad5">
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="msapplication-config" content="<?php echo Yii::getAlias('@web/media/favicons/browserconfig.xml');?>">
    <meta name="theme-color" content="#ffffff">

    <!-- localStorage test -->
    <script>!function(t){function r(){var t=navigator.userAgent,r=!window.addEventListener||t.match(/(Android (2|3|4.0|4.1|4.2|4.3))|(Opera (Mini|Mobi))/)&&!t.match(/Chrome/);if(r)return!1;var e="test";try{return localStorage.setItem(e,e),localStorage.removeItem(e),!0}catch(o){return!1}}t.localSupport=r(),t.localWrite=function(t,r){try{localStorage.setItem(t,r)}catch(e){if(e==QUOTA_EXCEEDED_ERR)return!1}}}(window);
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="page-wrapper">
    <!-- page-header -->
    <?php echo \frontend\components\Widgets::get('Header');?>
    <!-- end page-header -->

    <!-- page-container -->
    <div class="page-container">

        <section class="slider-block">

            <div class="slider single-item">
                <div><img src="/media/pic/slider-back.jpg"></div>
                <div><img src="/media/pic/slider-back2.jpg"></div>
            </div>

            <div class="slider-text">

                <h2>Утепление стен <span>пеноизолом</span></h2>

                <p>Если вы живете в частном доме или только строите его, то наверняка задумывались над утеплением стен</p>

                <div class="slider-button">
                    <a href="#"><span>Подробнее</span></a>
                </div>

            </div>

        </section>


        <section class="services">
            <div class="page-size">
                <h2>
                    Наши услуги
                </h2>

                <div class="clearfix">
                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 62px; height: 52px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg2"></use>
                                </svg>
                            </div>
                            <p>Утепление стен</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 92px; height: 48px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg3"></use>
                                </svg>
                            </div>
                            <p>Утепление кровли</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 69px; height: 47px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg1"></use>
                                </svg>
                            </div>
                            <p>Утепление чердака</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 55px; height: 54px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg1"></use>
                                </svg>
                            </div>
                            <p>Утепление пола</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 54px; height: 52px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg2"></use>
                                </svg>
                            </div>
                            <p>Утепление перегородки</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 59px; height: 55px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg1"></use>
                                </svg>
                            </div>
                            <p>Утепление промышленных зданий</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 72px; height: 72px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg3"></use>
                                </svg>
                            </div>
                            <p>Заполнение деформационных швов</p>
                        </a>
                    </div>

                    <div class="services__tile">
                        <a href="#">
                            <div class="services__image">
                                <svg style="width: 47px; height: 53px;">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/media/pic/sprite.svg#svg2"></use>
                                </svg>
                            </div>
                            <p>Вызов мастера</p>
                        </a>
                    </div>
                </div>


                <div class="services__button">
                    <a href="#popup-callback" class="popup-with-form"><span>Заказать услугу</span></a>
                </div>
            </div>
        </section>


        <section class="video-block">
            <div class="page-size">

                <div class="video-logo">
                    <img src="/media/pic/video-logo.png">
                </div>

                <h2>
                    Утепление домов жидким пенопластом
                </h2>

                <div class="video-play">
                    <a class="video-play popup-youtube" href="http://www.youtube.com/watch?v=0O2aH4XLbto">
                        <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                            <path d="M959,2129a24,24,0,1,1,24-24A24.029,24.029,0,0,1,959,2129Zm0-46.08A22.08,22.08,0,1,0,981.08,2105,22.1,22.1,0,0,0,959,2082.92Zm-6.24,35.31v-26.46L970.815,2105Zm1.92-22.68v18.89l12.888-9.44Z" transform="translate(-935 -2081)"/>
                        </svg>
                    </a>
                </div>

                <p>
                    <a class="video-play popup-youtube" href="http://www.youtube.com/watch?v=0O2aH4XLbto">Смотрите видео о нас</a>
                </p>


            </div>
        </section>



        <section class="advantages">
            <div class="advantages-city">
                <div class="page-size">
                    <h2>Преимущества</h2>

                    <div class="advantages__tile advantages__tile--left">
                        <div class="advantages__image">
                            <img src="/media/pic/advantage-1.jpg">
                        </div>
                        <p>Профессионализм</p>
                    </div>

                    <div class="advantages__tile advantages__tile--right">
                        <div class="advantages__image">
                            <img src="/media/pic/advantage-4.jpg">
                        </div>
                        <p>Проф. подход к организации проведения работ на объектах</p>
                    </div>

                    <div class="advantages__tile advantages__tile--left">
                        <div class="advantages__image">
                            <img src="/media/pic/advantage-2.jpg">
                        </div>
                        <p>Профессиональное оборудование</p>
                    </div>

                    <div class="advantages__tile advantages__tile--right">
                        <div class="advantages__image">
                            <img src="/media/pic/advantage-5.jpg">
                        </div>
                        <p>Качество</p>
                    </div>

                    <div class="advantages__tile advantages__tile--left">
                        <div class="advantages__image">
                            <img src="/media/pic/advantage-3.jpg">
                        </div>
                        <p>Высоколе качество материала</p>
                    </div>

                    <div class="advantages__tile advantages__tile--right">
                        <div class="advantages__image">
                            <img src="/media/pic/advantage-6.jpg">
                        </div>
                        <p>Надежность</p>
                    </div>

                    <div class="advantages-window">
                        <img src="/media/pic/advantages-window.png">
                    </div>

                    <h3>
                        Мы позаботимся <span>о тепле и уюте Вашего дома!</span>
                    </h3>

                    <div class="advantages__button" >
                        <a href="#popup-count" class="popup-with-form"><span>Заказать расчет утепления</span></a>
                    </div>

                </div>
            </div>
        </section>





        <section class="articles">
            <div class="page-size">
                <div class="articles-inner">
                    <div class="articles__news">
                        <h4>Свежие статьи</h4>
                        <div class="news__inner">
                            <div class="news">

                                <a href="#">
                                    <div class="news__date">
                                        <span>17</span> апреля
                                    </div>
                                </a>

                                <div class="news__text">
                                    <p>Вы приняли решение утеплить дом!?</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>

                            <div class="news">

                                <a href="#">
                                    <div class="news__date">
                                        <span>17</span> апреля
                                    </div>
                                </a>

                                <div class="news__text">
                                    <p>Вы приняли решение утеплить дом!?</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>

                            <div class="news">

                                <a href="#">
                                    <div class="news__date">
                                        <span>17</span> апреля
                                    </div>
                                </a>

                                <div class="news__text">
                                    <p>Вы приняли решение утеплить дом!?</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>

                            <div class="news">

                                <a href="#">
                                    <div class="news__date">
                                        <span>17</span> апреля
                                    </div>
                                </a>

                                <div class="news__text">
                                    <p>Вы приняли решение утеплить дом!?</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="articles__projects">
                        <h4>Свежие проекты</h4>
                        <div class="project__inner">
                            <div class="project">

                                <div class="project-image">
                                    <a href="#"><img src="/media/pic/project-1.jpg"></a>
                                </div>

                                <div class="project__text">
                                    <p>Утепление крыши пеноизолом</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>

                            <div class="project">

                                <div class="project-image">
                                    <a href="#"><img src="/media/pic/project-2.jpg"></a>
                                </div>

                                <div class="project__text">
                                    <p>Утепление крыши пеноизолом</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>

                            <div class="project">

                                <div class="project-image">
                                    <a href="#"><img src="/media/pic/project-3.jpg"></a>
                                </div>

                                <div class="project__text">
                                    <p>Утепление крыши пеноизолом</p>
                                    <a href="#">Подробнее</a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="articles__info">

                        <a href="#popup-count" class="popup-with-form">
                            <div class="articles__button">
                                <p><span>Заказать</span> расчет утепления</p>
                            </div>
                        </a>

                        <div class="articles__email">

                            <img src="/media/pic/articles-email.png">
                            <a href="mailto:info@insroi.com.ua">
                                <p>info@instroi.com.ua</p>
                            </a>

                        </div>

                        <div class="articles__call">

                            <img src="/media/pic/articles-call.png">
                            <ul>
                                <li><a href="tel:+380950000157">+38(095) 0000 157</a></li>
                                <li><a href="tel:+380676254579">+38(067) 6254 579</a></li>
                                <li><a href="tel:+380960000157">+38(096) 0000 157</a></li>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>
        </section>





        <section class="seo">
            <div class="page-size">
                <h3>Утепление стен пеноизолом</h3>

                <p>Если вы живете в частном доме или только строите его, то наверняка задумывались над утеплением стен. Ведь так важно чтобы в доме было тепло и уютно зимой, а летом сохранялась прохлада. Современный рынок предлагает нам всевозможные материалы для утепления и у каждого есть свои достоинства и недостатки.</p>

                <p>Самым экономичным, универсальным и эффективным способом считается утепление Пеноизолом. Этот материал обладает отличными тепло- и звукоизоляционными свойствами. Жидкий пенопласт является экологически чистым, не вреден для здоровья и окружающей среды. Вы можете не переживать о сохранности вашего дома, так как этот материал не горит, а грызунам и прочим вредителям он «придется не по вкусу». Еще одним достоинством Пеноизола является быстрота в исполнении работ по утеплению и невысокая цена, по сравнению с другими утеплителями. А срок его службы более 50 лет! Все эти свойства и делают Пеноизол отличным теплоизоляционным материалом. </p>

                <p>Пеноизол изготавливается прямо на месте выполнения работ, что дает возможность существенно сэкономить деньги на транспортировке материала. Сам процесс можно разделить на два вида, в зависимости от состояния здания. Например, если дом только на стадии строительства, то утеплитель заливается через открытое пространство между стенами. А если утеплить нужно жилой дом, то для этого понадобится просверлить небольшие отверстия.</p>
            </div>
        </section>

    </div>
    <!-- end page-container -->

    <!-- page-footer -->
    <footer class="page-footer">
        <div class="page-size">
            <div class="page-footer__inner">

                <div class="footer-logo">
                    <img src="/media/pic/footer-logo.png">

                    <div class="footer__copyright">
                        &copy 2016 OOO «Инстрой» | Утепление пеноизолом (заливочным пенопластом)
                    </div>

                    <div class="footer__map">
                        <a href="sitemap.html">Карта сайта</a>
                    </div>
                </div>

                <div class="footer-navigation">
                    <h6>Навигация</h6>

                    <ul>
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Услуги</a></li>
                        <li><a href="#">О пеноизоле</a></li>
                        <li><a href="#">Статьи</a></li>
                        <li><a href="#">Вопрос/Ответ</a></li>
                        <li><a href="#">Портфолио</a></li>
                        <li><a href="#">Прайс лист</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </div>


                <div class="footer__contacts">
                    <h6>Контакты</h6>

                    <div class="footer__contactsBlock">
                        <img src="/media/pic/footer-email.png">
                        <a href="mailto:info@insroi.com.ua"><p>info@insroi.com.ua</p></a>
                    </div>

                    <div class="footer__contactsBlock">
                        <img src="/media/pic/footer-call.png" class="footer__contactsBlock--image">
                        <ul>
                            <li><a href="tel:+380950000157">+38(095) 0000 157</a></li>
                            <li><a href="tel:+380676254579">+38(067) 6254 579</a></li>
                            <li><a href="tel:+380960000157">+38(096) 0000 157</a></li>
                        </ul>
                    </div>

                </div>


                <div class="footer__address">

                    <h6>Главный офис</h6>

                    <p>Киевская обл.,г.Боярка,
                        ул.Магистральная,179</p>

                    <h6>Филиал</h6>

                    <p>г.Мариуполь,
                        ул.Магистральная,179</p>

                </div>


                <div class="footer-socials">

                    <div class="footer__social">

                        <p>Мы в соцсетях</p>

                        <div class="footer__icons">
                            <a href="#"><img src="/media/pic/footer-fb.png"></a>
                            <a href="#"><img src="/media/pic/footer-google.png"></a>
                            <a href="#"><img src="/media/pic/footer-vk.png"></a>
                        </div>

                    </div>

                    <div class="footer__wezom">
                        <a href="http://wezom.com.ua/" target="_blank">
                            <p>Создание сайта -
                                агенство WEZOM</p>
                            <img src="/media/pic/footer-wezom.png">
                        </a>

                    </div>

                </div>

            </div>
        </div>
    </footer>
    <!-- end page-footer -->

</div>

<!-- noscript disabled message -->
<noscript>
    <link rel="stylesheet" property="stylesheet" href="<?php echo Yii::getAlias('@web/media/css/noscript-msg.css');?>">
    <input id="noscript-msg__close" type="checkbox" title="Закрити">
    <div id="noscript-msg">
        <label id="noscript-msg__times" for="noscript-msg__close" title="Закрити">&times;</label><a href="http://wezom.com.ua/" target="_blank" title="Cтудія Wezom" id="noscript-msg__link">&nbsp;</a>
        <div id="noscript-msg__block">
            <div id="noscript-msg__text">
                <p>У Вашому браузері відключений <strong>JavaScript</strong>! Для коректної роботи з сайтом необхідна підтримка Javascript.</p>
                <p>Ми рекомендуємо Вам включити використання JavaScript в налаштуваннях вашого браузера.</p>
            </div>
        </div>
    </div>
</noscript>

<div class="popup-wrapper">

    <div class="popup" id="popup-count">

        <div class="popup-upperBlock">
            <h4>Заказать расчёт</h4>

            <p>Мы свяжемся с Вами и все расскажем</p>
        </div>
        <div class="popup-lowerBlock wForm" data-form="true">
            <div class="popup__inputBlock">
                <label for="input__name">Ваше имя *</label><br>
                <input required type="text" name="word" id="input__name" class="popup__input" data-rule-word="true" data-rule-minlength="2">
            </div>

            <div class="popup__inputBlock">
                <label for="input__number">Номер телефона *</label><br>
                <input required type="tel" name="phoneUA" id="input__number" class="popup__input" data-rule-phoneUA="true">
            </div>

            <div class="popup__inputBlock">
                <label for="input__number">Услуга</label><br>
                <select class="popup__input">
                    <option value="0"></option>
                    <option value="1">Утепление стен</option>
                    <option value="2">Утепление кровли</option>
                    <option value="3">Утепление чердака</option>
                    <option value="4">Утепление пола</option>
                    <option value="5">Утепление перегородки</option>
                    <option value="6">Утепление промышленных зданий</option>
                    <option value="7">Заполнение деформационных швов</option>
                    <option value="8">Вызов мастера</option>
                </select>
            </div>

            <div class="popup__inputBlock">
                <label for="input__email">Ваш e-mail</label><br>
                <input type="email" name="email" id="input__email" class="popup__input" data-rule-email="true">
            </div>

            <div class="popup__inputBlock">
                <label for="input__message">Ваше сообщение</label><br>
                <textarea id="input__message" class="popup__input"></textarea>
            </div>

            <div class="popup__button wSubmit">
                <a>Заказать расчёт</a>
            </div>
        </div>

    </div>

    <div class="popup" id="popup-callback">

        <div class="popup-upperBlock">
            <h4>Обратный звонок</h4>

            <p>Мы свяжемся с Вами и все расскажем</p>
        </div>

        <div class="popup-lowerBlock wForm" data-form="true">

            <div class="popup__inputBlock">
                <label for="input__name">Ваше имя *</label><br>
                <input required type="text" name="word" id="input__name" class="popup__input" data-rule-word="true" data-rule-minlength="2">
            </div>

            <div class="popup__inputBlock">
                <label for="input__number">Номер телефона *</label><br>
                <input required type="tel" name="phoneUA" id="input__number" class="popup__input" data-rule-phoneUA="true">
            </div>

            <div class="popup__button popup__button--w220 wSubmit">
                <a>Перезвоните мне</a>
            </div>

        </div>
    </div>


</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
