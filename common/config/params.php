<?php
return [
    'images' => [
        'user' => [
            [
                'folder' => 'small',
                'width' => 160,
                'height' => 160,
            ],

            [
                'folder' => 'big',
                'width' => 500,
                'height' => 500,
            ],

            [
                'folder' => 'original'
            ]
        ],

        'slider' => [
            [
                'folder' => 'small',
                'width' => 329,
                'height' => 120,
            ],

            [
                'folder' => 'big',
                'width' => 1920,
                'height' => 700,
            ],

            [
                'folder' => 'original'
            ]
        ],
    ],
];
