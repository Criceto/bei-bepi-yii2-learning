<?php
namespace common\components;

use yii\base\Component;

class Text extends Component{

    public static function plural_form($string, $ch1, $ch2, $ch3){
        $ff=Array('0','1','2','3','4','5','6','7','8','9');
        if(substr($string,-2, 1)==1 AND strlen($string)>1) $ry=array("0 $ch3","1 $ch3","2 $ch3","3 $ch3" ,"4 $ch3","5 $ch3","6 $ch3","7 $ch3","8 $ch3","9 $ch3");
        else $ry=array("0 $ch3","1 $ch1","2 $ch2","3 $ch2","4 $ch2","5 $ch3"," 6 $ch3","7 $ch3","8 $ch3"," 9 $ch3");
        $string1=substr($string,0,-1).str_replace($ff, $ry, substr($string,-1,1));
        return $string1;
    }

    public static function translit($phrase)
    {
        $ru = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'і', 'є', 'ї', 'ґ', ' ', '"', "'", "`", ':', '«', '»', '.', ',', '’', '„', '”', '(', ')', '[', ']', '*', '@', '#', '“', '№', '%'];
        $en = ['a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'ju', 'ja', 'i', 'je', 'ji', 'g', '-', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'N', ''];
        $phrase = mb_strtolower($phrase, "UTF-8");
        $phrase = str_replace($ru, $en, $phrase);
        $phrase = preg_replace('/[^a-z0-9_-]/', '', $phrase);
        return $phrase;
    }
    
}