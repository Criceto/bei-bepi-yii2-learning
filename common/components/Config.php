<?php
namespace common\components;

use yii\base\Component;
use yii\db\Query;

class Config extends Component{
    static $_instance; // Constant that consists self class

    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public function init()
    {
        $result = (new Query())
            ->select([
                'config.*',
                'group_alias' => 'config_groups.alias'
            ])
            ->from('config')
            ->join('JOIN','config_groups', 'config_groups.id=config.group_id')
            ->where(['config.status' =>1])
            ->all();

        foreach ($result as $obj){
            \Yii::$app->params[$obj['group_alias'].'.'.$obj['key']] = $obj['value'];
        }


        parent::init();
    }
    
    public static function get($key){
        Config::factory();

        if(\Yii::$app->params[$key]) {
            return \Yii::$app->params[$key];
        }else {
            $keys = explode('.',$key);

            $result = \Yii::$app->params;
            foreach ($keys as $obj){
                $result = $result[$obj];
            }

            return $result;
        }

        return false;
    }

}