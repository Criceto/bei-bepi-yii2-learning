<?php
namespace common\components;

use app\modules\admin\models\Skills;
use common\models\User;
use frontend\components\widgets\Widgets;
use yii\base\Component;
use yii\helpers\Url;

class Digest extends Component{

    public static function createNewTicketAsAdmin($ticket_model){
        //создаём новую модель уведомления
        $digest_model = new \app\models\Digest();
        $digest_model->user_id = $ticket_model->user_id;
        $digest_model->from_user = \Yii::$app->user->identity->id;

        //получаем навык и пользователя
        $skill = null;
        if($ticket_model->skill_id) {
            $skill = Skills::findOne(['id' => $ticket_model->skill_id]);
        }
        
        switch ($ticket_model->status){
            case 0:
                if($ticket_model->user_id) {
                    $digest_model->short_text = 'Новая задача на изучение навыка '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->message = 'Администратором '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                        ' создана новая задача на изучение навыка '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                    return $digest_model->save();
                }
                break;
            case 1:
                if($ticket_model->user_id) {
                    $digest_model->short_text = 'Получено новое задание '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->message = 'Администратором '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                        ' выдано новое задание для изучения навыка '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                    return $digest_model->save();
                }
                break;
            case 2:
                if($ticket_model->user_id) {
                    $digest_model->short_text = 'Выполнено новое задание '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->message = 'Администратором '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                        ' задание '.($skill ? '"'.$skill->name.'" ' : ' ') . 'отмечено как выполненое.';
                    $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                    return $digest_model->save();
                }
                break;
            case 3:
                if($ticket_model->user_id) {
                    $digest_model->short_text = 'Задание '.($skill ? '"'.$skill->name.'" ' : ' ') . 'поступило на проверку.';
                    $digest_model->message = 'Администратор '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                        ' принял на проверку задание '.($skill ? $skill->name.'.' : '.');
                    $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                    return $digest_model->save();
                }
                break;
            case 4:
                if($ticket_model->user_id) {
                    $digest_model->short_text = 'Заявка '.($skill ? '"'.$skill->name.'" ' : ' ') . 'отменена.';
                    $digest_model->message = 'Администратор '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                        ' отменил заявку на выполнение задания '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                    return $digest_model->save();
                }
                break;
            case 5:
                if($ticket_model->user_id) {
                    $digest_model->short_text = 'Закрыта заявка'.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->message = 'Администратор '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                        ' закрыл заявку на выполнение задания '.($skill ? '"'.$skill->name.'".' : '.');
                    $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                    return $digest_model->save();
                }
                break;
        }
    }

    public static function changeTicketAsAdmin($ticket_model,$old_status){
        if($ticket_model->status != $old_status){

            //создаём новую модель уведомления
            $digest_model = new \app\models\Digest();
            $digest_model->user_id = $ticket_model->user_id;
            $digest_model->from_user = \Yii::$app->user->identity->id;

            //получаем навык и пользователя
            $skill = null;
            if($ticket_model->skill_id) {
                $skill = Skills::findOne(['id' => $ticket_model->skill_id]);
            }

            switch ($ticket_model->status){
                case 0:
                    if($ticket_model->user_id) {
                        $digest_model->short_text = 'Статус задачи '. ($skill ? '"'.$skill->name.'"' : '') .' изменён на "Новая"';
                        $digest_model->message = 'Администратором '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                            ' изменён статус задачи '.($skill ? '"'.$skill->name.'" ' : ' ') . 'на "Новая"';
                        $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                        return $digest_model->save();
                    }
                    break;
                case 1:
                    if($ticket_model->user_id) {
                        $digest_model->short_text = 'Получено новое задание '.($skill ? '"'.$skill->name.'".' : '.');
                        $digest_model->message = 'Администратором '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                            ' выдано новое задание для изучения навыка '.($skill ? '"'.$skill->name.'".' : '.');
                        $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                        return $digest_model->save();
                    }
                    break;
                case 2:
                    if($ticket_model->user_id) {
                        $digest_model->short_text = 'Статус задачи '.($skill ? '"'.$skill->name.'" ' : ' ') . 'изменён на "Выполнено"';
                        $digest_model->message = 'Администратором '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                            ' задание '.($skill ? '"'.$skill->name.'" ' : ' ') . 'отмечено как выполненое.';
                        $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                        return $digest_model->save();
                    }
                    break;
                case 3:
                    if($ticket_model->user_id) {
                        $digest_model->short_text = 'Задание '.($skill ? '"'.$skill->name.'" ' : ' ') . 'поступило на проверку.';
                        $digest_model->message = 'Администратор '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                            ' принял на проверку задание '.($skill ? '"'.$skill->name.'".' : '.');
                        $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                        return $digest_model->save();
                    }
                    break;
                case 4:
                    if($ticket_model->user_id) {
                        $digest_model->short_text = 'Заявка '.($skill ? '"'.$skill->name.'" ' : ' ') . 'отменена.';
                        $digest_model->message = 'Администратор '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                            ' отменил заявку на выполнение задания '.($skill ? '"'.$skill->name.'".' : '.');
                        $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                        return $digest_model->save();
                    }
                    break;
                case 5:
                    if($ticket_model->user_id) {
                        $digest_model->short_text = 'Закрыта заявка'.($skill ? '"'.$skill->name.'".' : '.');
                        $digest_model->message = 'Администратор '.(\Yii::$app->user->identity->name ? \Yii::$app->user->identity->name : \Yii::$app->user->identity->email).
                            ' закрыл заявку на выполнение задания '.($skill ? '"'.$skill->name.'".' : '.');
                        $digest_model->link = Url::toRoute(['/user/tickets/update','id' => $ticket_model->id]);
                        return $digest_model->save();
                    }
                    break;
            }
        }

        return false;
    }
    
    public static function sendEmails(){
        $users = User::find()->where(['status' => 1])->all();

        foreach ($users as $obj){
            $messages= \app\models\Digest::getUserMessages($obj->id);
            if(count($messages)){
                $table = Widgets::get('MailTemplates_DigestTable', [
                    'messages' => $messages
                ]);
                $user_name = $obj->name ? $obj->name : $obj->email;

                Email::sendToCron(2,[
                    '{{user_name}}',
                    '{{table}}'
                ], [
                    $user_name,
                    $table
                ],$obj->email);
            }
        }
        
        return true;
    }

}