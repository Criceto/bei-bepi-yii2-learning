<?php
namespace common\components;

use yii\base\Component;

	
    class HTML extends Component{



        public static function get_new_url($param,$url = null){

            if($url==null) {
                //получаем текущий урл страницы
                $old_url = explode("?", $_SERVER['REQUEST_URI']);
            } else {
                $old_url = explode("?",$url);
            }

            //формируем новый урл
            $new_url = $old_url[0]."?";

            //указатель, если параметры существуют, а такого ещё нет
            $u = false;

            //если существуют параметры разбиваем параметры
            if(isset($old_url[1])) {
                $old_params = explode("&", $old_url[1]);
                $newPar = explode('=',$param);
                //пробегаемся по параметрами проверяем, существует ли такой параметр, если да, то заменяем его
                for($i=1;$i<count($old_params);$i++){
                    //разбиваем параметр на левую и правую часть
                    $par = explode('=',$old_params[$i]);
                    if($par[0]=="page" && $newPar[0]=="at_page"){
                        $par[1]=1;
                    }
                    if($par[0]==$newPar[0]){
                        $par[1] = $newPar[1];
                        $u = true;
                    }

                    $new_url .= "&".$par[0]."=".$par[1];

                }

                if(!$u){
                    $new_url .= "&".$param;
                }
                //если параметров ещё нет
            } else {
                $new_url .= "&".$param;
            }



            return $new_url;

        }
        /**
         *  Generate good link. Useful in multi language sites
         *  @param  string $link - link
         *  @return string       - good link
         */
        public static function link( $link = '', $http = false ) {
            $link = trim($link, '/');
            if(strpos($link, 'http://') !== false) {
                return $link;
            }
            if ($link == 'index') { $link = ''; }
            if($http) {
                return 'http://'.$_SERVER['HTTP_HOST'].'/'.trim($link, '/');
            }
            return '/'.trim($link, '/');
        }


        /**
         * Create path to media in frontend
         * @param  string $file - path to file
         * @param  bool $absolute - use absolute path
         * @return string
         */
        public static function media( $file, $absolute = false ) {
            if($absolute) {
                return 'http://'.$_SERVER['HTTP_HOST'].'/Media/'.trim($file, '/');
            }
            return '/Media/' . trim($file, '/');
        }


        /**
         * Emulation of php function getallheaders()
         */
        public static function emu_getallheaders() {
            foreach($_SERVER as $h=>$v)
            if(ereg('HTTP_(.+)',$h,$hp))
                $headers[$hp[1]]=$v;
            return $headers;
        }


        /**
         * Convert special characters to HTML entities. All untrusted content
         * should be passed through this method to prevent XSS injections.
         *
         *     echo HTML::chars($username);
         *
         * @param   string  $value          string to convert
         * @param   boolean $double_encode  encode existing entities
         * @return  string
         */
        public static function chars($value, $double_encode = TRUE) {
            return htmlspecialchars( (string) $value, ENT_QUOTES, 'UTF-8', $double_encode);
        }


        /**
         * Creates a style sheet link element.
         *
         *     echo HTML::style('media/css/screen.css');
         *
         * @param   string  $file       file name
         * @param   array   $attributes default attributes
         * @param   mixed   $protocol   protocol to pass to URL::base()
         * @return  string
         * @uses    URL::base
         * @uses    Form::attributes
         */



        /**
         * Compress html page
         * @param $html
         * @param boolean $insolently
         * @return mixed
         */
        public static function compress($html, $insolently = false) {
            if((int) Config::get('speed.compress') || $insolently) {
                $html = preg_replace('/[\r\n\t]+/', ' ', $html);
                $html = preg_replace('/[\s]+/', ' ', $html);
                $html = preg_replace("/\> \</", "><", $html);
                $html = preg_replace("/\<!--[^\[*?\]].*?--\>/", "", $html);
            }
            return $html;
        }

    }