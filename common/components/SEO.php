<?php
namespace common\components;

use yii\base\Component;

class SEO extends Component{
    
    
    public static function setSeoParams($_seo = []){
        if($_seo['title']){
            \Yii::$app->view->title = $_seo['title'];
        }

        if($_seo['description']){
            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $_seo['description']
            ]);
        }

        if($_seo['keywords']){
            \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $_seo['keywords']
            ]);
        }
    }
    
}