<?php
namespace common\components;

use yii\base\Component;

class Dates extends Component{

    public static function timeAgo($time_ago)
    {
        $time_ago = $time_ago;
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "Только что";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "Минуту назад";
            }
            else{
                return Text::plural_form((string)$minutes, 'минута', 'минуты', 'минут');
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "Час назад";
            }else{
                return Text::plural_form((string)$hours,'час','часа','часов');
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "Вчера";
            }else{
                return Text::plural_form((string)$days,'день', 'дня', 'дней');
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "Неделю назад";
            }else{
                return Text::plural_form((string)$weeks, 'неделя', 'недели', 'недель');
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "Месяц назад";
            }else{
                return Text::plural_form((string)$months,'месяц','месяца','месяцев');
            }
        }
        //Years
        else{
            if($years==1){
                return "Год назад";
            }else{
                return Text::plural_form((string)$years,'год', 'года', 'лет');
            }
        }
    }

}