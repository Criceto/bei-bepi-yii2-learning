<?php
namespace common\components;

use yii\base\Component;

    /**
     *  Example:
     *  $pager = Pager::factory(3, 10, 2)->create();
     */    
    class Pager extends Component{

        public $_current; // Current page
        public $_next; // Next page
        public $_previous; // Previous page
        public $_total; // Total count of items
        public $_per_page; // Count of items per page
        public $_total_pages; // Total count of pages

        public $_uri; // Current URI before "?"
        public $_get; // Part of URI after "?" and with "?"

        public $_count_out = 3; // Number of page links in the begin and end of whole range
        public $_count_in = 2; // Number of page links on each side of current page
        public $_navigation = TRUE;


        public static function factory( $current, $total, $per_page ) {
            return new Pager( $current, $total, $per_page );
        }


        public function __construct( $current, $total, $per_page ) {
            if(!$per_page) {
                return NULL;
            }
            try {
                $this->_current = (int)$current;
                $this->_total = (int)$total;
                $this->_per_page = (int)$per_page;
                $this->_total_pages = ceil($this->_total / $this->_per_page);
                if ($this->_navigation) {
                    $this->_next = $this->_current < $this->_total_pages ? $this->_current + 1 : FALSE;
                    $this->_previous = $this->_current > 1 ? $this->_current - 1 : FALSE;
                }
                $this->setURI();
            }
            catch (\Exception $e) {
                die($e->getMessage());
            }
        }


        public function setURI() {
            $uri = $_SERVER['REQUEST_URI'];
            $get_page = $_GET['page'];
            $uri = str_replace('page='.$get_page, '', $uri);
            $uri = explode('?', $uri);
            $this->_get = $uri[1] ? '?'.$uri[1] : NULL;
            $this->_uri = $uri[0];
            if ( preg_match('/\/page\/'.$this->_current.'/', $this->_uri, $matches) ) {
                $this->_uri = str_replace($matches, '', $this->_uri);
            }
        }


        public function create() {
            if ($this->_total_pages < 2) { return ''; }
            $data = array();
            foreach ($this as $key => $value) {
                $data[$key] = $value;
            }
            $data['page'] = $this;
            return \Yii::$app->view->render('@app/views/widgets/PagerView.php', $data);
        }


        public function url( $page = 1 ) {
            // Clean the page number
            $page = max(1, (int) $page);

            // No page number in URLs to first page
            if ($page === 1) {
                return $this->_uri.$this->_get;
            }

            // Generate URI with new page
            return $this->_uri.'?page='.$page.$this->_get;
        }

    }