<?php
namespace common\components;

use yii\base\Component;

class Notify extends Component{

    public static function setMessage($type='success', $message=null){
        if($message){
            \Yii::$app->session->setFlash($type,$message);
        }
    }
    
    public static function getMessage(){
        if(\Yii::$app->session->getFlash('success')){
            return [
                'type' => 'success',
                'message' => \Yii::$app->session->getFlash('success')
            ];
        }

        if(\Yii::$app->session->getFlash('warning')){
            return [
                'type' => 'warning',
                'message' => \Yii::$app->session->getFlash('warning')
            ];
        }

        if(\Yii::$app->session->getFlash('error')){
            return [
                'type' => 'error',
                'message' => \Yii::$app->session->getFlash('error')
            ];
        }
    }

}