<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mail_templates".
 *
 * @property integer $id
 * @property string $name
 * @property string $subject
 * @property string $text
 * @property integer $updated_at
 * @property integer $status
 * @property integer $sort
 */
class MailTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mail_templates';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['updated_at', 'status', 'sort'], 'integer'],
            [['name', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'subject' => 'Тема письма',
            'text' => 'Текст',
            'updated_at' => 'Updated At',
            'status' => 'Опубликовано',
            'sort' => 'Sort',
        ];
    }
}
