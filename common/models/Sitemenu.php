<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sitemenu".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $url
 * @property integer $created_at
 * @property integer $sort
 * @property integer $updated_at
 */
class Sitemenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sitemenu';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'sort'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
            ['name', 'required', 'message' => 'Укажите название!'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Опубликован?',
            'url' => 'Ссылка',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sort' => 'Sort',
        ];
    }
}
