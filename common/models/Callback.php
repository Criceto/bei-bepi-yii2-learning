<?php

namespace common\models;

use common\components\Email;
use common\components\System;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "callback".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $ip
 * @property integer $status
 * @property integer $sort
 * @property integer $created_at
 * @property integer $updated_at
 */
class Callback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'callback';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'sort', 'created_at', 'updated_at'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя пользователя',
            'phone' => 'Телефон',
            'ip' => 'Ip адрес',
            'status' => 'Прочитано?',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function saveFromFrontForm(){

        $this->name = Yii::$app->request->post('name');
        if(!strlen(trim($this->name))){
            return ['success' => false, 'fields' => ['name' => true]];
        }

        $this->phone = Yii::$app->request->post('phone');
        if(strlen(trim($this->phone)) < 6){
            return ['success' => false, 'fields' => ['phone' => true]];
        }

        $this->ip = System::getRealIP();

        $this->save();

        Email::send(1,[
            '{{name}}',
            '{{phone}}'
        ], [
            $this->name,
            $this->phone
        ]);
        
        return ['success' => true];
    }
}
