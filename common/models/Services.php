<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $text
 * @property string $text_short
 * @property string $text_short2
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property string $tags
 * @property string $image2
 * @property string $icon
 * @property string $h1
 * @property string $title
 * @property string $keywords
 * @property string $description
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'text_short', 'text_short2', 'keywords', 'description', 'icon'], 'string'],
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['name', 'alias', 'image', 'tags', 'image2', 'h1', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'text' => 'Содержание',
            'text_short' => 'Краткое описание на списке',
            'text_short2' => 'Краткое описание на изображении',
            'image' => 'Изображение на списках',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Опубликован?',
            'tags' => 'Теги',
            'image2' => 'Изображение на внутренней странице',
            'icon' => 'Иконка',
            'h1' => 'H1',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
        ];
    }
}
