<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "control".
 *
 * @property integer $id
 * @property string $name
 * @property string $h1
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $text1
 * @property string $alias
 * @property integer $status
 * @property string $other
 * @property string $string1
 * @property string $string2
 * @property string $string3
 * @property string $image
 * @property string $text2
 */
class Control extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'control';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keywords', 'description', 'text', 'other'], 'string'],
            [['status'], 'integer'],
            [['name', 'h1', 'title', 'image'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'h1' => 'H1',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'text' => 'СЕО текст',
            'alias' => 'Alias',
            'status' => 'Опубликован',
            'other' => 'Другое',
            'image' => 'Image',
        ];
    }
}
