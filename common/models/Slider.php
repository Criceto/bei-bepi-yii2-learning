<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $name
 * @property string $name2
 * @property string $text
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $sort
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['created_at', 'updated_at', 'status', 'sort'], 'integer'],
            [['name', 'name2', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название - первая строка',
            'name2' => 'Название - вторая строка',
            'text' => 'Описание',
            'image' => 'Изображение',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Опубликован?',
            'sort' => 'Sort',
        ];
    }
}
